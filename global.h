/* 
 * File:   global.h
 * Author: rtiong
 *
 * Created on February 9, 2022, 10:29 AM
 */

#include <xc.h>

#include "channel.h"
#include "zcb.h"
#include "buffer.h"


#ifndef GLOBAL_H
#define	GLOBAL_H

#define _XTAL_FREQ 1000000

#define SOFTWARE_TYPE   'D'     //'D'evelopment or 'R'elease 
#define SOFTWARE_NUM    84   //VERSION
#define SOFTWARE_VER    0x0222  //MMYY

#define ZCB_TYPE1   0x0F    // 4 zone control board
#define ZCB_TYPE2   0x3F    // 6 zones control board

#define ZONE_1    0b00000001
#define ZONE_2    0b00000010
#define ZONE_3    0b00000100
#define ZONE_4    0b00001000
#define ZONE_5    0b00010000
#define ZONE_6    0b00100000
#define ZONE_ALL  0b00111111

#define MODBUS_ID_NULL  0xFF

// Zone Control Board IDs
#define MODBUS_ID_ZCB1  212
#define MODBUS_ID_ZCB2  213
#define MODBUS_ID_ZCB3  214

// Valid air sensors ID
#define MODBUS_ID_AS0   165
#define MODBUS_ID_AS1   166
#define MODBUS_ID_AS2   167
#define MODBUS_ID_AS3   168
#define MODBUS_ID_AS4   169
#define MODBUS_ID_AS5   170
#define MODBUS_ID_AS6   171
#define MODBUS_ID_AS7   172
#define MODBUS_ID_AS8   173
#define MODBUS_ID_AS9   174

// Valid zone wall controllers ID
#define MODBUS_ID_SWC0  150
#define MODBUS_ID_SWC1  151
#define MODBUS_ID_SWC2  152
#define MODBUS_ID_SWC3  153
#define MODBUS_ID_SWC4  154
#define MODBUS_ID_SWC5  155
#define MODBUS_ID_SWC6  156
#define MODBUS_ID_SWC7  157
#define MODBUS_ID_SWC8  158
#define MODBUS_ID_SWC9  159

// ZCB registers
#define R40250  249
#define R40251  250
#define R40252  251
#define R40253  252
#define R40254  253
#define R40255  254

#define R_ID_SWC  204 // 40205 register stores zone wall control id
#define R_ID_AS   299 // 40300 register store air sensor id



/*** MACROS ***/
// State controls
#define IDLE    0xFF    // do nothing
#define REQUEST 0xF0    // request for a task
#define RUN     0x11    // processing task
#define DONE    0x12    // task completed

// Flag controls
#define FLAG_SET    0xA5
#define FLAG_RESET  0xFF

// Boolean
#define TRUE        0xA5
#define FALSE       0xFF

#define RD_FUNCTION 0x03
#define WR_FUNCTION 0x10

#define BUFFER_SIZE 128

//uint16_t copy_sreg, copy_nreg;


//-------------OBJECTS---------------------------------------------
//-------------OBJECTS---------------------------------------------
//-------------OBJECTS---------------------------------------------
//-------------OBJECTS---------------------------------------------
//-------------OBJECTS---------------------------------------------
ZCB zcb1, zcb2, zcb3;
Channel channel1, channel2, channel3, channel4, channel5, channel6;
Buffer main_rx, main_tx, sub_rx, sub_tx;

uint8_t ZCB_type;

uint8_t zcb3_active = FALSE;


#endif	/* GLOBAL_H */

