/* 
 * File:   modbus_sub.h
 * Author: rtiong
 *
 * Created on March 3, 2022, 4:01 PM
 */

#ifndef MODBUS_H
#define	MODBUS_H

#include "global.h"
#include <xc.h>

void MODBUS_Initialize(void);
void MODBUS_Update(void);
#endif	/* MODBUS_H */

