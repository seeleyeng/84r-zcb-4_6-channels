/**
  TMR0 Generated Driver File

  @Company
    Microchip Technology Inc.

  @File Name
    tmr0.c

  @Summary
    This is the generated driver implementation file for the TMR0 driver using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  @Description
    This source file provides APIs for TMR0.
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.81.7
        Device            :  PIC18F47Q10
        Driver Version    :  3.10
    The generated drivers are tested against the following:
        Compiler          :  XC8 2.31 and above
        MPLAB 	          :  MPLAB X 5.45
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

/**
  Section: Included Files
*/

#include <xc.h>
#include "tmr0.h"
#include "../global.h"

uint8_t time_flag = 0x00;

extern uint8_t main_connect;
extern uint8_t main_connect_cnt;

extern uint8_t MAP_led;
extern uint8_t MAP_dampers;
extern uint8_t MAP_switches;

uint8_t timer_1ms_cnt = 0;
uint8_t timer_10ms_cnt = 0;
uint8_t timer_100ms_cnt = 0;
uint8_t timer_1s_cnt = 0;

/**
  Section: TMR0 APIs
*/

void (*TMR0_InterruptHandler)(void);

void TMR0_Initialize(void)
{
    // Set TMR0 to the options selected in the User Interface

    // T0CS FOSC/4; T0CKPS 1:1; T0ASYNC synchronised; 
    T0CON1 = 0x40;

    // TMR0H 199; 
    TMR0H = 0xA8;

    // TMR0L 0; 
    TMR0L = 0x00;

    // Clear Interrupt flag before enabling the interrupt
    PIR0bits.TMR0IF = 0;

    // Enabling TMR0 interrupt.
    PIE0bits.TMR0IE = 1;

    // Set Default Interrupt Handler
    //TMR0_SetInterruptHandler(TMR0_DefaultInterruptHandler);

    // T0OUTPS 1:1; T0EN enabled; T016BIT 8-bit; 
    T0CON0 = 0x80;
    
    // reset timers
    timer_100ms_cnt = 0;
    timer_10ms_cnt = 0;
    timer_1s_cnt = 0;
}

void TMR0_Reset()
{
    // TMR0L 0; 
    TMR0L = 0x00;
}

void TMR0_ISR(void)
{
    
    PIR0bits.TMR0IF = 0;
    TMR0_Reset();
    
    //UPDATES EVERY 1MS---updates every 1ms
    timer_10ms_cnt++;

    if(timer_10ms_cnt > 100)
    {
        Buffer_tick(&main_rx);
        Buffer_tick(&sub_rx);

        timer_100ms_cnt++;
        timer_10ms_cnt = 0;
    }
    //UPDATES EVERY 100MS---updates every 100ms
    if(timer_100ms_cnt >= 10)
    {
        timer_100ms_cnt = 0;
        timer_1s_cnt++;
        if(main_connect_cnt > 0) main_connect_cnt--;
    }
    
    if(timer_1s_cnt > 10)
    {
        timer_1s_cnt = 0;
    }
    
    if(timer_1s_cnt > 5)
    {
        if(main_connect == FALSE) MAP_led &= ~MASK_ON;
        else MAP_led |= MASK_ON;
    }
    else
    {
        MAP_led |= MASK_ON;
    }
    
    
    Channel_tickSw(&channel1);
    Channel_tickSw(&channel2);
    Channel_tickSw(&channel3);
    Channel_tickSw(&channel4);
    Channel_tickSw(&channel5);
    Channel_tickSw(&channel6); 
//    Buffer_tick(&main_tx);
//    Buffer_tick(&sub_tx);
   
//    MAP_led ^= MASK_VS;
   // PortA_out = (PortB_in & 0x0F);
   // PortA_out = (MAP_dampers & ZCB_type);
    MAP_led ^= MASK_VS;
    PortD_out = MAP_led;   // leds output
}

/*
void TMR0_SetInterruptHandler(void (* InterruptHandler)(void)){
    TMR0_InterruptHandler = InterruptHandler;
}

void TMR0_DefaultInterruptHandler(void){
    // add your TMR0 interrupt custom code
    // or set custom function using TMR0_SetInterruptHandler()
}
*/
/**
  End of File
*/