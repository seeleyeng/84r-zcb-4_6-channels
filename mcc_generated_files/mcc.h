/**
  @Generated PIC10 / PIC12 / PIC16 / PIC18 MCUs Header File

  @Company:
    Microchip Technology Inc.

  @File Name:
    mcc.h

  @Summary:
    This is the mcc.h file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  @Description:
    This header file provides implementations for driver APIs for all modules selected in the GUI.
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.81.7
        Device            :  PIC18F47Q10
        Driver Version    :  2.00
    The generated drivers are tested against the following:
        Compiler          :  XC8 2.31 and above or later
        MPLAB             :  MPLAB X 5.45
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

#ifndef MCC_H
#define	MCC_H
#include <xc.h>
#include <stdint.h>
#include <stdbool.h>
#include <conio.h>
#include "interrupt_manager.h"
#include "tmr0.h"
#include "eusart1.h"
#include "eusart2.h"
#include "pin_manager.h"


// pin configurations
#define Damper1     LATAbits.LATA0
#define Damper2     LATAbits.LATA1
#define Damper3     LATAbits.LATA2
#define Damper4     LATAbits.LATA3
#define Damper5     LATAbits.LATA4
#define Damper6     LATAbits.LATA5
// #define spare    LATAbits.LATA6
// #define spare    LATAbits.LATA7
#define PortA_out   LATA

#define Switch1     PORTBbits.RB0
#define Switch2     PORTBbits.RB1
#define Switch3     PORTBbits.RB2
#define Switch4     PORTBbits.RB3
#define Switch5     PORTBbits.RB4
#define Switch6     PORTBbits.RB5
#define tx2         PORTBbits.RB6
#define rx2         PORTBbits.RB7
#define PortB_in    PORTB

#define DE_COM1     LATCbits.LATC0
#define DE_COM2     LATCbits.LATC1
#define DE_COM3     LATCbits.LATC2
#define DE_COM4     LATCbits.LATC3
#define DE_COM5     LATCbits.LATC4
#define DE_COM6     LATCbits.LATC5
#define DE_COM      LATC
// #define TxS    LATCbits.LATC6
// #define RxS    LATCbits.LATC7

#define LED_C1      LATDbits.LATD0
#define LED_C2      LATDbits.LATD1
#define LED_C3      LATDbits.LATD2
#define LED_C4      LATDbits.LATD3
#define LED_C5      LATDbits.LATD4
#define LED_C6      LATDbits.LATD5
#define LED_PWR     LATDbits.LATD6
#define EN_Vs       LATDbits.LATD7
#define PortD_out   LATD


#define DE_Main   LATEbits.LATE0
#define Switch_ID   PORTEbits.RE1
#define Switch_PB   PORTEbits.RE2
#define PortE_in    PORTE

#define MASK_ON    0b01000000   // led power
#define MASK_VS    0b10000000   // supply to zone controllers

#define MASK_ID    0b00000010   // zcb id
#define MASK_PB    0b00000100   // push button


/**
 * @Param
    none
 * @Returns
    none
 * @Description
    Initializes the device to the default states configured in the
 *                  MCC GUI
 * @Example
    SYSTEM_Initialize(void);
 */
void SYSTEM_Initialize(void);

/**
 * @Param
    none
 * @Returns
    none
 * @Description
    Initializes the oscillator to the default states configured in the
 *                  MCC GUI
 * @Example
    OSCILLATOR_Initialize(void);
 */

void OSCILLATOR_Initialize(void);

/**
 * @Param
    none
 * @Returns
    none
 * @Description
    Initializes the PMD module to the default states configured in the
 *                  MCC GUI
 * @Example
    PMD_Initialize(void);
 */
void PMD_Initialize(void);


#endif	/* MCC_H */
/**
 End of File
*/