/**
  @Generated Pin Manager Header File

  @Company:
    Microchip Technology Inc.

  @File Name:
    pin_manager.h

  @Summary:
    This is the Pin Manager file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  @Description
    This header file provides APIs for driver for .
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.81.7
        Device            :  PIC18F47Q10
        Driver Version    :  2.11
    The generated drivers are tested against the following:
        Compiler          :  XC8 2.31 and above
        MPLAB 	          :  MPLAB X 5.45	
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

#ifndef PIN_MANAGER_H
#define PIN_MANAGER_H

/**
  Section: Included Files
*/

#include <xc.h>

#define INPUT   1
#define OUTPUT  0

#define HIGH    1
#define LOW     0

#define ANALOG      1
#define DIGITAL     0

#define PULL_UP_ENABLED      1
#define PULL_UP_DISABLED     0

// get/set Damper1 aliases
#define Damper1_TRIS                 TRISAbits.TRISA0
#define Damper1_LAT                  LATAbits.LATA0
#define Damper1_PORT                 PORTAbits.RA0
#define Damper1_WPU                  WPUAbits.WPUA0
#define Damper1_OD                   ODCONAbits.ODCA0
#define Damper1_ANS                  ANSELAbits.ANSELA0
#define Damper1_SetHigh()            do { LATAbits.LATA0 = 1; } while(0)
#define Damper1_SetLow()             do { LATAbits.LATA0 = 0; } while(0)
#define Damper1_Toggle()             do { LATAbits.LATA0 = ~LATAbits.LATA0; } while(0)
#define Damper1_GetValue()           PORTAbits.RA0
#define Damper1_SetDigitalInput()    do { TRISAbits.TRISA0 = 1; } while(0)
#define Damper1_SetDigitalOutput()   do { TRISAbits.TRISA0 = 0; } while(0)
#define Damper1_SetPullup()          do { WPUAbits.WPUA0 = 1; } while(0)
#define Damper1_ResetPullup()        do { WPUAbits.WPUA0 = 0; } while(0)
#define Damper1_SetPushPull()        do { ODCONAbits.ODCA0 = 0; } while(0)
#define Damper1_SetOpenDrain()       do { ODCONAbits.ODCA0 = 1; } while(0)
#define Damper1_SetAnalogMode()      do { ANSELAbits.ANSELA0 = 1; } while(0)
#define Damper1_SetDigitalMode()     do { ANSELAbits.ANSELA0 = 0; } while(0)

// get/set Damper2 aliases
#define Damper2_TRIS                 TRISAbits.TRISA1
#define Damper2_LAT                  LATAbits.LATA1
#define Damper2_PORT                 PORTAbits.RA1
#define Damper2_WPU                  WPUAbits.WPUA1
#define Damper2_OD                   ODCONAbits.ODCA1
#define Damper2_ANS                  ANSELAbits.ANSELA1
#define Damper2_SetHigh()            do { LATAbits.LATA1 = 1; } while(0)
#define Damper2_SetLow()             do { LATAbits.LATA1 = 0; } while(0)
#define Damper2_Toggle()             do { LATAbits.LATA1 = ~LATAbits.LATA1; } while(0)
#define Damper2_GetValue()           PORTAbits.RA1
#define Damper2_SetDigitalInput()    do { TRISAbits.TRISA1 = 1; } while(0)
#define Damper2_SetDigitalOutput()   do { TRISAbits.TRISA1 = 0; } while(0)
#define Damper2_SetPullup()          do { WPUAbits.WPUA1 = 1; } while(0)
#define Damper2_ResetPullup()        do { WPUAbits.WPUA1 = 0; } while(0)
#define Damper2_SetPushPull()        do { ODCONAbits.ODCA1 = 0; } while(0)
#define Damper2_SetOpenDrain()       do { ODCONAbits.ODCA1 = 1; } while(0)
#define Damper2_SetAnalogMode()      do { ANSELAbits.ANSELA1 = 1; } while(0)
#define Damper2_SetDigitalMode()     do { ANSELAbits.ANSELA1 = 0; } while(0)

// get/set Damper3 aliases
#define Damper3_TRIS                 TRISAbits.TRISA2
#define Damper3_LAT                  LATAbits.LATA2
#define Damper3_PORT                 PORTAbits.RA2
#define Damper3_WPU                  WPUAbits.WPUA2
#define Damper3_OD                   ODCONAbits.ODCA2
#define Damper3_ANS                  ANSELAbits.ANSELA2
#define Damper3_SetHigh()            do { LATAbits.LATA2 = 1; } while(0)
#define Damper3_SetLow()             do { LATAbits.LATA2 = 0; } while(0)
#define Damper3_Toggle()             do { LATAbits.LATA2 = ~LATAbits.LATA2; } while(0)
#define Damper3_GetValue()           PORTAbits.RA2
#define Damper3_SetDigitalInput()    do { TRISAbits.TRISA2 = 1; } while(0)
#define Damper3_SetDigitalOutput()   do { TRISAbits.TRISA2 = 0; } while(0)
#define Damper3_SetPullup()          do { WPUAbits.WPUA2 = 1; } while(0)
#define Damper3_ResetPullup()        do { WPUAbits.WPUA2 = 0; } while(0)
#define Damper3_SetPushPull()        do { ODCONAbits.ODCA2 = 0; } while(0)
#define Damper3_SetOpenDrain()       do { ODCONAbits.ODCA2 = 1; } while(0)
#define Damper3_SetAnalogMode()      do { ANSELAbits.ANSELA2 = 1; } while(0)
#define Damper3_SetDigitalMode()     do { ANSELAbits.ANSELA2 = 0; } while(0)

// get/set Damper4 aliases
#define Damper4_TRIS                 TRISAbits.TRISA3
#define Damper4_LAT                  LATAbits.LATA3
#define Damper4_PORT                 PORTAbits.RA3
#define Damper4_WPU                  WPUAbits.WPUA3
#define Damper4_OD                   ODCONAbits.ODCA3
#define Damper4_ANS                  ANSELAbits.ANSELA3
#define Damper4_SetHigh()            do { LATAbits.LATA3 = 1; } while(0)
#define Damper4_SetLow()             do { LATAbits.LATA3 = 0; } while(0)
#define Damper4_Toggle()             do { LATAbits.LATA3 = ~LATAbits.LATA3; } while(0)
#define Damper4_GetValue()           PORTAbits.RA3
#define Damper4_SetDigitalInput()    do { TRISAbits.TRISA3 = 1; } while(0)
#define Damper4_SetDigitalOutput()   do { TRISAbits.TRISA3 = 0; } while(0)
#define Damper4_SetPullup()          do { WPUAbits.WPUA3 = 1; } while(0)
#define Damper4_ResetPullup()        do { WPUAbits.WPUA3 = 0; } while(0)
#define Damper4_SetPushPull()        do { ODCONAbits.ODCA3 = 0; } while(0)
#define Damper4_SetOpenDrain()       do { ODCONAbits.ODCA3 = 1; } while(0)
#define Damper4_SetAnalogMode()      do { ANSELAbits.ANSELA3 = 1; } while(0)
#define Damper4_SetDigitalMode()     do { ANSELAbits.ANSELA3 = 0; } while(0)

// get/set Damper5 aliases
#define Damper5_TRIS                 TRISAbits.TRISA4
#define Damper5_LAT                  LATAbits.LATA4
#define Damper5_PORT                 PORTAbits.RA4
#define Damper5_WPU                  WPUAbits.WPUA4
#define Damper5_OD                   ODCONAbits.ODCA4
#define Damper5_ANS                  ANSELAbits.ANSELA4
#define Damper5_SetHigh()            do { LATAbits.LATA4 = 1; } while(0)
#define Damper5_SetLow()             do { LATAbits.LATA4 = 0; } while(0)
#define Damper5_Toggle()             do { LATAbits.LATA4 = ~LATAbits.LATA4; } while(0)
#define Damper5_GetValue()           PORTAbits.RA4
#define Damper5_SetDigitalInput()    do { TRISAbits.TRISA4 = 1; } while(0)
#define Damper5_SetDigitalOutput()   do { TRISAbits.TRISA4 = 0; } while(0)
#define Damper5_SetPullup()          do { WPUAbits.WPUA4 = 1; } while(0)
#define Damper5_ResetPullup()        do { WPUAbits.WPUA4 = 0; } while(0)
#define Damper5_SetPushPull()        do { ODCONAbits.ODCA4 = 0; } while(0)
#define Damper5_SetOpenDrain()       do { ODCONAbits.ODCA4 = 1; } while(0)
#define Damper5_SetAnalogMode()      do { ANSELAbits.ANSELA4 = 1; } while(0)
#define Damper5_SetDigitalMode()     do { ANSELAbits.ANSELA4 = 0; } while(0)

// get/set Damper6 aliases
#define Damper6_TRIS                 TRISAbits.TRISA5
#define Damper6_LAT                  LATAbits.LATA5
#define Damper6_PORT                 PORTAbits.RA5
#define Damper6_WPU                  WPUAbits.WPUA5
#define Damper6_OD                   ODCONAbits.ODCA5
#define Damper6_ANS                  ANSELAbits.ANSELA5
#define Damper6_SetHigh()            do { LATAbits.LATA5 = 1; } while(0)
#define Damper6_SetLow()             do { LATAbits.LATA5 = 0; } while(0)
#define Damper6_Toggle()             do { LATAbits.LATA5 = ~LATAbits.LATA5; } while(0)
#define Damper6_GetValue()           PORTAbits.RA5
#define Damper6_SetDigitalInput()    do { TRISAbits.TRISA5 = 1; } while(0)
#define Damper6_SetDigitalOutput()   do { TRISAbits.TRISA5 = 0; } while(0)
#define Damper6_SetPullup()          do { WPUAbits.WPUA5 = 1; } while(0)
#define Damper6_ResetPullup()        do { WPUAbits.WPUA5 = 0; } while(0)
#define Damper6_SetPushPull()        do { ODCONAbits.ODCA5 = 0; } while(0)
#define Damper6_SetOpenDrain()       do { ODCONAbits.ODCA5 = 1; } while(0)
#define Damper6_SetAnalogMode()      do { ANSELAbits.ANSELA5 = 1; } while(0)
#define Damper6_SetDigitalMode()     do { ANSELAbits.ANSELA5 = 0; } while(0)

// get/set Switch1 aliases
#define Switch1_TRIS                 TRISBbits.TRISB0
#define Switch1_LAT                  LATBbits.LATB0
#define Switch1_PORT                 PORTBbits.RB0
#define Switch1_WPU                  WPUBbits.WPUB0
#define Switch1_OD                   ODCONBbits.ODCB0
#define Switch1_ANS                  ANSELBbits.ANSELB0
#define Switch1_SetHigh()            do { LATBbits.LATB0 = 1; } while(0)
#define Switch1_SetLow()             do { LATBbits.LATB0 = 0; } while(0)
#define Switch1_Toggle()             do { LATBbits.LATB0 = ~LATBbits.LATB0; } while(0)
#define Switch1_GetValue()           PORTBbits.RB0
#define Switch1_SetDigitalInput()    do { TRISBbits.TRISB0 = 1; } while(0)
#define Switch1_SetDigitalOutput()   do { TRISBbits.TRISB0 = 0; } while(0)
#define Switch1_SetPullup()          do { WPUBbits.WPUB0 = 1; } while(0)
#define Switch1_ResetPullup()        do { WPUBbits.WPUB0 = 0; } while(0)
#define Switch1_SetPushPull()        do { ODCONBbits.ODCB0 = 0; } while(0)
#define Switch1_SetOpenDrain()       do { ODCONBbits.ODCB0 = 1; } while(0)
#define Switch1_SetAnalogMode()      do { ANSELBbits.ANSELB0 = 1; } while(0)
#define Switch1_SetDigitalMode()     do { ANSELBbits.ANSELB0 = 0; } while(0)

// get/set Switch2 aliases
#define Switch2_TRIS                 TRISBbits.TRISB1
#define Switch2_LAT                  LATBbits.LATB1
#define Switch2_PORT                 PORTBbits.RB1
#define Switch2_WPU                  WPUBbits.WPUB1
#define Switch2_OD                   ODCONBbits.ODCB1
#define Switch2_ANS                  ANSELBbits.ANSELB1
#define Switch2_SetHigh()            do { LATBbits.LATB1 = 1; } while(0)
#define Switch2_SetLow()             do { LATBbits.LATB1 = 0; } while(0)
#define Switch2_Toggle()             do { LATBbits.LATB1 = ~LATBbits.LATB1; } while(0)
#define Switch2_GetValue()           PORTBbits.RB1
#define Switch2_SetDigitalInput()    do { TRISBbits.TRISB1 = 1; } while(0)
#define Switch2_SetDigitalOutput()   do { TRISBbits.TRISB1 = 0; } while(0)
#define Switch2_SetPullup()          do { WPUBbits.WPUB1 = 1; } while(0)
#define Switch2_ResetPullup()        do { WPUBbits.WPUB1 = 0; } while(0)
#define Switch2_SetPushPull()        do { ODCONBbits.ODCB1 = 0; } while(0)
#define Switch2_SetOpenDrain()       do { ODCONBbits.ODCB1 = 1; } while(0)
#define Switch2_SetAnalogMode()      do { ANSELBbits.ANSELB1 = 1; } while(0)
#define Switch2_SetDigitalMode()     do { ANSELBbits.ANSELB1 = 0; } while(0)

// get/set Switch3 aliases
#define Switch3_TRIS                 TRISBbits.TRISB2
#define Switch3_LAT                  LATBbits.LATB2
#define Switch3_PORT                 PORTBbits.RB2
#define Switch3_WPU                  WPUBbits.WPUB2
#define Switch3_OD                   ODCONBbits.ODCB2
#define Switch3_ANS                  ANSELBbits.ANSELB2
#define Switch3_SetHigh()            do { LATBbits.LATB2 = 1; } while(0)
#define Switch3_SetLow()             do { LATBbits.LATB2 = 0; } while(0)
#define Switch3_Toggle()             do { LATBbits.LATB2 = ~LATBbits.LATB2; } while(0)
#define Switch3_GetValue()           PORTBbits.RB2
#define Switch3_SetDigitalInput()    do { TRISBbits.TRISB2 = 1; } while(0)
#define Switch3_SetDigitalOutput()   do { TRISBbits.TRISB2 = 0; } while(0)
#define Switch3_SetPullup()          do { WPUBbits.WPUB2 = 1; } while(0)
#define Switch3_ResetPullup()        do { WPUBbits.WPUB2 = 0; } while(0)
#define Switch3_SetPushPull()        do { ODCONBbits.ODCB2 = 0; } while(0)
#define Switch3_SetOpenDrain()       do { ODCONBbits.ODCB2 = 1; } while(0)
#define Switch3_SetAnalogMode()      do { ANSELBbits.ANSELB2 = 1; } while(0)
#define Switch3_SetDigitalMode()     do { ANSELBbits.ANSELB2 = 0; } while(0)

// get/set Switch4 aliases
#define Switch4_TRIS                 TRISBbits.TRISB3
#define Switch4_LAT                  LATBbits.LATB3
#define Switch4_PORT                 PORTBbits.RB3
#define Switch4_WPU                  WPUBbits.WPUB3
#define Switch4_OD                   ODCONBbits.ODCB3
#define Switch4_ANS                  ANSELBbits.ANSELB3
#define Switch4_SetHigh()            do { LATBbits.LATB3 = 1; } while(0)
#define Switch4_SetLow()             do { LATBbits.LATB3 = 0; } while(0)
#define Switch4_Toggle()             do { LATBbits.LATB3 = ~LATBbits.LATB3; } while(0)
#define Switch4_GetValue()           PORTBbits.RB3
#define Switch4_SetDigitalInput()    do { TRISBbits.TRISB3 = 1; } while(0)
#define Switch4_SetDigitalOutput()   do { TRISBbits.TRISB3 = 0; } while(0)
#define Switch4_SetPullup()          do { WPUBbits.WPUB3 = 1; } while(0)
#define Switch4_ResetPullup()        do { WPUBbits.WPUB3 = 0; } while(0)
#define Switch4_SetPushPull()        do { ODCONBbits.ODCB3 = 0; } while(0)
#define Switch4_SetOpenDrain()       do { ODCONBbits.ODCB3 = 1; } while(0)
#define Switch4_SetAnalogMode()      do { ANSELBbits.ANSELB3 = 1; } while(0)
#define Switch4_SetDigitalMode()     do { ANSELBbits.ANSELB3 = 0; } while(0)

// get/set Switch5 aliases
#define Switch5_TRIS                 TRISBbits.TRISB4
#define Switch5_LAT                  LATBbits.LATB4
#define Switch5_PORT                 PORTBbits.RB4
#define Switch5_WPU                  WPUBbits.WPUB4
#define Switch5_OD                   ODCONBbits.ODCB4
#define Switch5_ANS                  ANSELBbits.ANSELB4
#define Switch5_SetHigh()            do { LATBbits.LATB4 = 1; } while(0)
#define Switch5_SetLow()             do { LATBbits.LATB4 = 0; } while(0)
#define Switch5_Toggle()             do { LATBbits.LATB4 = ~LATBbits.LATB4; } while(0)
#define Switch5_GetValue()           PORTBbits.RB4
#define Switch5_SetDigitalInput()    do { TRISBbits.TRISB4 = 1; } while(0)
#define Switch5_SetDigitalOutput()   do { TRISBbits.TRISB4 = 0; } while(0)
#define Switch5_SetPullup()          do { WPUBbits.WPUB4 = 1; } while(0)
#define Switch5_ResetPullup()        do { WPUBbits.WPUB4 = 0; } while(0)
#define Switch5_SetPushPull()        do { ODCONBbits.ODCB4 = 0; } while(0)
#define Switch5_SetOpenDrain()       do { ODCONBbits.ODCB4 = 1; } while(0)
#define Switch5_SetAnalogMode()      do { ANSELBbits.ANSELB4 = 1; } while(0)
#define Switch5_SetDigitalMode()     do { ANSELBbits.ANSELB4 = 0; } while(0)

// get/set Switch6 aliases
#define Switch6_TRIS                 TRISBbits.TRISB5
#define Switch6_LAT                  LATBbits.LATB5
#define Switch6_PORT                 PORTBbits.RB5
#define Switch6_WPU                  WPUBbits.WPUB5
#define Switch6_OD                   ODCONBbits.ODCB5
#define Switch6_ANS                  ANSELBbits.ANSELB5
#define Switch6_SetHigh()            do { LATBbits.LATB5 = 1; } while(0)
#define Switch6_SetLow()             do { LATBbits.LATB5 = 0; } while(0)
#define Switch6_Toggle()             do { LATBbits.LATB5 = ~LATBbits.LATB5; } while(0)
#define Switch6_GetValue()           PORTBbits.RB5
#define Switch6_SetDigitalInput()    do { TRISBbits.TRISB5 = 1; } while(0)
#define Switch6_SetDigitalOutput()   do { TRISBbits.TRISB5 = 0; } while(0)
#define Switch6_SetPullup()          do { WPUBbits.WPUB5 = 1; } while(0)
#define Switch6_ResetPullup()        do { WPUBbits.WPUB5 = 0; } while(0)
#define Switch6_SetPushPull()        do { ODCONBbits.ODCB5 = 0; } while(0)
#define Switch6_SetOpenDrain()       do { ODCONBbits.ODCB5 = 1; } while(0)
#define Switch6_SetAnalogMode()      do { ANSELBbits.ANSELB5 = 1; } while(0)
#define Switch6_SetDigitalMode()     do { ANSELBbits.ANSELB5 = 0; } while(0)

// get/set RB6 procedures
#define RB6_SetHigh()            do { LATBbits.LATB6 = 1; } while(0)
#define RB6_SetLow()             do { LATBbits.LATB6 = 0; } while(0)
#define RB6_Toggle()             do { LATBbits.LATB6 = ~LATBbits.LATB6; } while(0)
#define RB6_GetValue()              PORTBbits.RB6
#define RB6_SetDigitalInput()    do { TRISBbits.TRISB6 = 1; } while(0)
#define RB6_SetDigitalOutput()   do { TRISBbits.TRISB6 = 0; } while(0)
#define RB6_SetPullup()             do { WPUBbits.WPUB6 = 1; } while(0)
#define RB6_ResetPullup()           do { WPUBbits.WPUB6 = 0; } while(0)
#define RB6_SetAnalogMode()         do { ANSELBbits.ANSELB6 = 1; } while(0)
#define RB6_SetDigitalMode()        do { ANSELBbits.ANSELB6 = 0; } while(0)

// get/set RB7 procedures
#define RB7_SetHigh()            do { LATBbits.LATB7 = 1; } while(0)
#define RB7_SetLow()             do { LATBbits.LATB7 = 0; } while(0)
#define RB7_Toggle()             do { LATBbits.LATB7 = ~LATBbits.LATB7; } while(0)
#define RB7_GetValue()              PORTBbits.RB7
#define RB7_SetDigitalInput()    do { TRISBbits.TRISB7 = 1; } while(0)
#define RB7_SetDigitalOutput()   do { TRISBbits.TRISB7 = 0; } while(0)
#define RB7_SetPullup()             do { WPUBbits.WPUB7 = 1; } while(0)
#define RB7_ResetPullup()           do { WPUBbits.WPUB7 = 0; } while(0)
#define RB7_SetAnalogMode()         do { ANSELBbits.ANSELB7 = 1; } while(0)
#define RB7_SetDigitalMode()        do { ANSELBbits.ANSELB7 = 0; } while(0)

// get/set DE_COM1 aliases
#define DE_COM1_TRIS                 TRISCbits.TRISC0
#define DE_COM1_LAT                  LATCbits.LATC0
#define DE_COM1_PORT                 PORTCbits.RC0
#define DE_COM1_WPU                  WPUCbits.WPUC0
#define DE_COM1_OD                   ODCONCbits.ODCC0
#define DE_COM1_ANS                  ANSELCbits.ANSELC0
#define DE_COM1_SetHigh()            do { LATCbits.LATC0 = 1; } while(0)
#define DE_COM1_SetLow()             do { LATCbits.LATC0 = 0; } while(0)
#define DE_COM1_Toggle()             do { LATCbits.LATC0 = ~LATCbits.LATC0; } while(0)
#define DE_COM1_GetValue()           PORTCbits.RC0
#define DE_COM1_SetDigitalInput()    do { TRISCbits.TRISC0 = 1; } while(0)
#define DE_COM1_SetDigitalOutput()   do { TRISCbits.TRISC0 = 0; } while(0)
#define DE_COM1_SetPullup()          do { WPUCbits.WPUC0 = 1; } while(0)
#define DE_COM1_ResetPullup()        do { WPUCbits.WPUC0 = 0; } while(0)
#define DE_COM1_SetPushPull()        do { ODCONCbits.ODCC0 = 0; } while(0)
#define DE_COM1_SetOpenDrain()       do { ODCONCbits.ODCC0 = 1; } while(0)
#define DE_COM1_SetAnalogMode()      do { ANSELCbits.ANSELC0 = 1; } while(0)
#define DE_COM1_SetDigitalMode()     do { ANSELCbits.ANSELC0 = 0; } while(0)

// get/set DE_COM2 aliases
#define DE_COM2_TRIS                 TRISCbits.TRISC1
#define DE_COM2_LAT                  LATCbits.LATC1
#define DE_COM2_PORT                 PORTCbits.RC1
#define DE_COM2_WPU                  WPUCbits.WPUC1
#define DE_COM2_OD                   ODCONCbits.ODCC1
#define DE_COM2_ANS                  ANSELCbits.ANSELC1
#define DE_COM2_SetHigh()            do { LATCbits.LATC1 = 1; } while(0)
#define DE_COM2_SetLow()             do { LATCbits.LATC1 = 0; } while(0)
#define DE_COM2_Toggle()             do { LATCbits.LATC1 = ~LATCbits.LATC1; } while(0)
#define DE_COM2_GetValue()           PORTCbits.RC1
#define DE_COM2_SetDigitalInput()    do { TRISCbits.TRISC1 = 1; } while(0)
#define DE_COM2_SetDigitalOutput()   do { TRISCbits.TRISC1 = 0; } while(0)
#define DE_COM2_SetPullup()          do { WPUCbits.WPUC1 = 1; } while(0)
#define DE_COM2_ResetPullup()        do { WPUCbits.WPUC1 = 0; } while(0)
#define DE_COM2_SetPushPull()        do { ODCONCbits.ODCC1 = 0; } while(0)
#define DE_COM2_SetOpenDrain()       do { ODCONCbits.ODCC1 = 1; } while(0)
#define DE_COM2_SetAnalogMode()      do { ANSELCbits.ANSELC1 = 1; } while(0)
#define DE_COM2_SetDigitalMode()     do { ANSELCbits.ANSELC1 = 0; } while(0)

// get/set DE_COM3 aliases
#define DE_COM3_TRIS                 TRISCbits.TRISC2
#define DE_COM3_LAT                  LATCbits.LATC2
#define DE_COM3_PORT                 PORTCbits.RC2
#define DE_COM3_WPU                  WPUCbits.WPUC2
#define DE_COM3_OD                   ODCONCbits.ODCC2
#define DE_COM3_ANS                  ANSELCbits.ANSELC2
#define DE_COM3_SetHigh()            do { LATCbits.LATC2 = 1; } while(0)
#define DE_COM3_SetLow()             do { LATCbits.LATC2 = 0; } while(0)
#define DE_COM3_Toggle()             do { LATCbits.LATC2 = ~LATCbits.LATC2; } while(0)
#define DE_COM3_GetValue()           PORTCbits.RC2
#define DE_COM3_SetDigitalInput()    do { TRISCbits.TRISC2 = 1; } while(0)
#define DE_COM3_SetDigitalOutput()   do { TRISCbits.TRISC2 = 0; } while(0)
#define DE_COM3_SetPullup()          do { WPUCbits.WPUC2 = 1; } while(0)
#define DE_COM3_ResetPullup()        do { WPUCbits.WPUC2 = 0; } while(0)
#define DE_COM3_SetPushPull()        do { ODCONCbits.ODCC2 = 0; } while(0)
#define DE_COM3_SetOpenDrain()       do { ODCONCbits.ODCC2 = 1; } while(0)
#define DE_COM3_SetAnalogMode()      do { ANSELCbits.ANSELC2 = 1; } while(0)
#define DE_COM3_SetDigitalMode()     do { ANSELCbits.ANSELC2 = 0; } while(0)

// get/set DE_COM4 aliases
#define DE_COM4_TRIS                 TRISCbits.TRISC3
#define DE_COM4_LAT                  LATCbits.LATC3
#define DE_COM4_PORT                 PORTCbits.RC3
#define DE_COM4_WPU                  WPUCbits.WPUC3
#define DE_COM4_OD                   ODCONCbits.ODCC3
#define DE_COM4_ANS                  ANSELCbits.ANSELC3
#define DE_COM4_SetHigh()            do { LATCbits.LATC3 = 1; } while(0)
#define DE_COM4_SetLow()             do { LATCbits.LATC3 = 0; } while(0)
#define DE_COM4_Toggle()             do { LATCbits.LATC3 = ~LATCbits.LATC3; } while(0)
#define DE_COM4_GetValue()           PORTCbits.RC3
#define DE_COM4_SetDigitalInput()    do { TRISCbits.TRISC3 = 1; } while(0)
#define DE_COM4_SetDigitalOutput()   do { TRISCbits.TRISC3 = 0; } while(0)
#define DE_COM4_SetPullup()          do { WPUCbits.WPUC3 = 1; } while(0)
#define DE_COM4_ResetPullup()        do { WPUCbits.WPUC3 = 0; } while(0)
#define DE_COM4_SetPushPull()        do { ODCONCbits.ODCC3 = 0; } while(0)
#define DE_COM4_SetOpenDrain()       do { ODCONCbits.ODCC3 = 1; } while(0)
#define DE_COM4_SetAnalogMode()      do { ANSELCbits.ANSELC3 = 1; } while(0)
#define DE_COM4_SetDigitalMode()     do { ANSELCbits.ANSELC3 = 0; } while(0)

// get/set DE_COM5 aliases
#define DE_COM5_TRIS                 TRISCbits.TRISC4
#define DE_COM5_LAT                  LATCbits.LATC4
#define DE_COM5_PORT                 PORTCbits.RC4
#define DE_COM5_WPU                  WPUCbits.WPUC4
#define DE_COM5_OD                   ODCONCbits.ODCC4
#define DE_COM5_ANS                  ANSELCbits.ANSELC4
#define DE_COM5_SetHigh()            do { LATCbits.LATC4 = 1; } while(0)
#define DE_COM5_SetLow()             do { LATCbits.LATC4 = 0; } while(0)
#define DE_COM5_Toggle()             do { LATCbits.LATC4 = ~LATCbits.LATC4; } while(0)
#define DE_COM5_GetValue()           PORTCbits.RC4
#define DE_COM5_SetDigitalInput()    do { TRISCbits.TRISC4 = 1; } while(0)
#define DE_COM5_SetDigitalOutput()   do { TRISCbits.TRISC4 = 0; } while(0)
#define DE_COM5_SetPullup()          do { WPUCbits.WPUC4 = 1; } while(0)
#define DE_COM5_ResetPullup()        do { WPUCbits.WPUC4 = 0; } while(0)
#define DE_COM5_SetPushPull()        do { ODCONCbits.ODCC4 = 0; } while(0)
#define DE_COM5_SetOpenDrain()       do { ODCONCbits.ODCC4 = 1; } while(0)
#define DE_COM5_SetAnalogMode()      do { ANSELCbits.ANSELC4 = 1; } while(0)
#define DE_COM5_SetDigitalMode()     do { ANSELCbits.ANSELC4 = 0; } while(0)

// get/set DE_COM6 aliases
#define DE_COM6_TRIS                 TRISCbits.TRISC5
#define DE_COM6_LAT                  LATCbits.LATC5
#define DE_COM6_PORT                 PORTCbits.RC5
#define DE_COM6_WPU                  WPUCbits.WPUC5
#define DE_COM6_OD                   ODCONCbits.ODCC5
#define DE_COM6_ANS                  ANSELCbits.ANSELC5
#define DE_COM6_SetHigh()            do { LATCbits.LATC5 = 1; } while(0)
#define DE_COM6_SetLow()             do { LATCbits.LATC5 = 0; } while(0)
#define DE_COM6_Toggle()             do { LATCbits.LATC5 = ~LATCbits.LATC5; } while(0)
#define DE_COM6_GetValue()           PORTCbits.RC5
#define DE_COM6_SetDigitalInput()    do { TRISCbits.TRISC5 = 1; } while(0)
#define DE_COM6_SetDigitalOutput()   do { TRISCbits.TRISC5 = 0; } while(0)
#define DE_COM6_SetPullup()          do { WPUCbits.WPUC5 = 1; } while(0)
#define DE_COM6_ResetPullup()        do { WPUCbits.WPUC5 = 0; } while(0)
#define DE_COM6_SetPushPull()        do { ODCONCbits.ODCC5 = 0; } while(0)
#define DE_COM6_SetOpenDrain()       do { ODCONCbits.ODCC5 = 1; } while(0)
#define DE_COM6_SetAnalogMode()      do { ANSELCbits.ANSELC5 = 1; } while(0)
#define DE_COM6_SetDigitalMode()     do { ANSELCbits.ANSELC5 = 0; } while(0)

// get/set RC6 procedures
#define RC6_SetHigh()            do { LATCbits.LATC6 = 1; } while(0)
#define RC6_SetLow()             do { LATCbits.LATC6 = 0; } while(0)
#define RC6_Toggle()             do { LATCbits.LATC6 = ~LATCbits.LATC6; } while(0)
#define RC6_GetValue()              PORTCbits.RC6
#define RC6_SetDigitalInput()    do { TRISCbits.TRISC6 = 1; } while(0)
#define RC6_SetDigitalOutput()   do { TRISCbits.TRISC6 = 0; } while(0)
#define RC6_SetPullup()             do { WPUCbits.WPUC6 = 1; } while(0)
#define RC6_ResetPullup()           do { WPUCbits.WPUC6 = 0; } while(0)
#define RC6_SetAnalogMode()         do { ANSELCbits.ANSELC6 = 1; } while(0)
#define RC6_SetDigitalMode()        do { ANSELCbits.ANSELC6 = 0; } while(0)

// get/set RC7 procedures
#define RC7_SetHigh()            do { LATCbits.LATC7 = 1; } while(0)
#define RC7_SetLow()             do { LATCbits.LATC7 = 0; } while(0)
#define RC7_Toggle()             do { LATCbits.LATC7 = ~LATCbits.LATC7; } while(0)
#define RC7_GetValue()              PORTCbits.RC7
#define RC7_SetDigitalInput()    do { TRISCbits.TRISC7 = 1; } while(0)
#define RC7_SetDigitalOutput()   do { TRISCbits.TRISC7 = 0; } while(0)
#define RC7_SetPullup()             do { WPUCbits.WPUC7 = 1; } while(0)
#define RC7_ResetPullup()           do { WPUCbits.WPUC7 = 0; } while(0)
#define RC7_SetAnalogMode()         do { ANSELCbits.ANSELC7 = 1; } while(0)
#define RC7_SetDigitalMode()        do { ANSELCbits.ANSELC7 = 0; } while(0)

// get/set LED_COM1 aliases
#define LED_COM1_TRIS                 TRISDbits.TRISD0
#define LED_COM1_LAT                  LATDbits.LATD0
#define LED_COM1_PORT                 PORTDbits.RD0
#define LED_COM1_WPU                  WPUDbits.WPUD0
#define LED_COM1_OD                   ODCONDbits.ODCD0
#define LED_COM1_ANS                  ANSELDbits.ANSELD0
#define LED_COM1_SetHigh()            do { LATDbits.LATD0 = 1; } while(0)
#define LED_COM1_SetLow()             do { LATDbits.LATD0 = 0; } while(0)
#define LED_COM1_Toggle()             do { LATDbits.LATD0 = ~LATDbits.LATD0; } while(0)
#define LED_COM1_GetValue()           PORTDbits.RD0
#define LED_COM1_SetDigitalInput()    do { TRISDbits.TRISD0 = 1; } while(0)
#define LED_COM1_SetDigitalOutput()   do { TRISDbits.TRISD0 = 0; } while(0)
#define LED_COM1_SetPullup()          do { WPUDbits.WPUD0 = 1; } while(0)
#define LED_COM1_ResetPullup()        do { WPUDbits.WPUD0 = 0; } while(0)
#define LED_COM1_SetPushPull()        do { ODCONDbits.ODCD0 = 0; } while(0)
#define LED_COM1_SetOpenDrain()       do { ODCONDbits.ODCD0 = 1; } while(0)
#define LED_COM1_SetAnalogMode()      do { ANSELDbits.ANSELD0 = 1; } while(0)
#define LED_COM1_SetDigitalMode()     do { ANSELDbits.ANSELD0 = 0; } while(0)

// get/set LED_COM2 aliases
#define LED_COM2_TRIS                 TRISDbits.TRISD1
#define LED_COM2_LAT                  LATDbits.LATD1
#define LED_COM2_PORT                 PORTDbits.RD1
#define LED_COM2_WPU                  WPUDbits.WPUD1
#define LED_COM2_OD                   ODCONDbits.ODCD1
#define LED_COM2_ANS                  ANSELDbits.ANSELD1
#define LED_COM2_SetHigh()            do { LATDbits.LATD1 = 1; } while(0)
#define LED_COM2_SetLow()             do { LATDbits.LATD1 = 0; } while(0)
#define LED_COM2_Toggle()             do { LATDbits.LATD1 = ~LATDbits.LATD1; } while(0)
#define LED_COM2_GetValue()           PORTDbits.RD1
#define LED_COM2_SetDigitalInput()    do { TRISDbits.TRISD1 = 1; } while(0)
#define LED_COM2_SetDigitalOutput()   do { TRISDbits.TRISD1 = 0; } while(0)
#define LED_COM2_SetPullup()          do { WPUDbits.WPUD1 = 1; } while(0)
#define LED_COM2_ResetPullup()        do { WPUDbits.WPUD1 = 0; } while(0)
#define LED_COM2_SetPushPull()        do { ODCONDbits.ODCD1 = 0; } while(0)
#define LED_COM2_SetOpenDrain()       do { ODCONDbits.ODCD1 = 1; } while(0)
#define LED_COM2_SetAnalogMode()      do { ANSELDbits.ANSELD1 = 1; } while(0)
#define LED_COM2_SetDigitalMode()     do { ANSELDbits.ANSELD1 = 0; } while(0)

// get/set LED_COM3 aliases
#define LED_COM3_TRIS                 TRISDbits.TRISD2
#define LED_COM3_LAT                  LATDbits.LATD2
#define LED_COM3_PORT                 PORTDbits.RD2
#define LED_COM3_WPU                  WPUDbits.WPUD2
#define LED_COM3_OD                   ODCONDbits.ODCD2
#define LED_COM3_ANS                  ANSELDbits.ANSELD2
#define LED_COM3_SetHigh()            do { LATDbits.LATD2 = 1; } while(0)
#define LED_COM3_SetLow()             do { LATDbits.LATD2 = 0; } while(0)
#define LED_COM3_Toggle()             do { LATDbits.LATD2 = ~LATDbits.LATD2; } while(0)
#define LED_COM3_GetValue()           PORTDbits.RD2
#define LED_COM3_SetDigitalInput()    do { TRISDbits.TRISD2 = 1; } while(0)
#define LED_COM3_SetDigitalOutput()   do { TRISDbits.TRISD2 = 0; } while(0)
#define LED_COM3_SetPullup()          do { WPUDbits.WPUD2 = 1; } while(0)
#define LED_COM3_ResetPullup()        do { WPUDbits.WPUD2 = 0; } while(0)
#define LED_COM3_SetPushPull()        do { ODCONDbits.ODCD2 = 0; } while(0)
#define LED_COM3_SetOpenDrain()       do { ODCONDbits.ODCD2 = 1; } while(0)
#define LED_COM3_SetAnalogMode()      do { ANSELDbits.ANSELD2 = 1; } while(0)
#define LED_COM3_SetDigitalMode()     do { ANSELDbits.ANSELD2 = 0; } while(0)

// get/set LED_COM4 aliases
#define LED_COM4_TRIS                 TRISDbits.TRISD3
#define LED_COM4_LAT                  LATDbits.LATD3
#define LED_COM4_PORT                 PORTDbits.RD3
#define LED_COM4_WPU                  WPUDbits.WPUD3
#define LED_COM4_OD                   ODCONDbits.ODCD3
#define LED_COM4_ANS                  ANSELDbits.ANSELD3
#define LED_COM4_SetHigh()            do { LATDbits.LATD3 = 1; } while(0)
#define LED_COM4_SetLow()             do { LATDbits.LATD3 = 0; } while(0)
#define LED_COM4_Toggle()             do { LATDbits.LATD3 = ~LATDbits.LATD3; } while(0)
#define LED_COM4_GetValue()           PORTDbits.RD3
#define LED_COM4_SetDigitalInput()    do { TRISDbits.TRISD3 = 1; } while(0)
#define LED_COM4_SetDigitalOutput()   do { TRISDbits.TRISD3 = 0; } while(0)
#define LED_COM4_SetPullup()          do { WPUDbits.WPUD3 = 1; } while(0)
#define LED_COM4_ResetPullup()        do { WPUDbits.WPUD3 = 0; } while(0)
#define LED_COM4_SetPushPull()        do { ODCONDbits.ODCD3 = 0; } while(0)
#define LED_COM4_SetOpenDrain()       do { ODCONDbits.ODCD3 = 1; } while(0)
#define LED_COM4_SetAnalogMode()      do { ANSELDbits.ANSELD3 = 1; } while(0)
#define LED_COM4_SetDigitalMode()     do { ANSELDbits.ANSELD3 = 0; } while(0)

// get/set LED_COM5 aliases
#define LED_COM5_TRIS                 TRISDbits.TRISD4
#define LED_COM5_LAT                  LATDbits.LATD4
#define LED_COM5_PORT                 PORTDbits.RD4
#define LED_COM5_WPU                  WPUDbits.WPUD4
#define LED_COM5_OD                   ODCONDbits.ODCD4
#define LED_COM5_ANS                  ANSELDbits.ANSELD4
#define LED_COM5_SetHigh()            do { LATDbits.LATD4 = 1; } while(0)
#define LED_COM5_SetLow()             do { LATDbits.LATD4 = 0; } while(0)
#define LED_COM5_Toggle()             do { LATDbits.LATD4 = ~LATDbits.LATD4; } while(0)
#define LED_COM5_GetValue()           PORTDbits.RD4
#define LED_COM5_SetDigitalInput()    do { TRISDbits.TRISD4 = 1; } while(0)
#define LED_COM5_SetDigitalOutput()   do { TRISDbits.TRISD4 = 0; } while(0)
#define LED_COM5_SetPullup()          do { WPUDbits.WPUD4 = 1; } while(0)
#define LED_COM5_ResetPullup()        do { WPUDbits.WPUD4 = 0; } while(0)
#define LED_COM5_SetPushPull()        do { ODCONDbits.ODCD4 = 0; } while(0)
#define LED_COM5_SetOpenDrain()       do { ODCONDbits.ODCD4 = 1; } while(0)
#define LED_COM5_SetAnalogMode()      do { ANSELDbits.ANSELD4 = 1; } while(0)
#define LED_COM5_SetDigitalMode()     do { ANSELDbits.ANSELD4 = 0; } while(0)

// get/set LED_COM6 aliases
#define LED_COM6_TRIS                 TRISDbits.TRISD5
#define LED_COM6_LAT                  LATDbits.LATD5
#define LED_COM6_PORT                 PORTDbits.RD5
#define LED_COM6_WPU                  WPUDbits.WPUD5
#define LED_COM6_OD                   ODCONDbits.ODCD5
#define LED_COM6_ANS                  ANSELDbits.ANSELD5
#define LED_COM6_SetHigh()            do { LATDbits.LATD5 = 1; } while(0)
#define LED_COM6_SetLow()             do { LATDbits.LATD5 = 0; } while(0)
#define LED_COM6_Toggle()             do { LATDbits.LATD5 = ~LATDbits.LATD5; } while(0)
#define LED_COM6_GetValue()           PORTDbits.RD5
#define LED_COM6_SetDigitalInput()    do { TRISDbits.TRISD5 = 1; } while(0)
#define LED_COM6_SetDigitalOutput()   do { TRISDbits.TRISD5 = 0; } while(0)
#define LED_COM6_SetPullup()          do { WPUDbits.WPUD5 = 1; } while(0)
#define LED_COM6_ResetPullup()        do { WPUDbits.WPUD5 = 0; } while(0)
#define LED_COM6_SetPushPull()        do { ODCONDbits.ODCD5 = 0; } while(0)
#define LED_COM6_SetOpenDrain()       do { ODCONDbits.ODCD5 = 1; } while(0)
#define LED_COM6_SetAnalogMode()      do { ANSELDbits.ANSELD5 = 1; } while(0)
#define LED_COM6_SetDigitalMode()     do { ANSELDbits.ANSELD5 = 0; } while(0)

// get/set LED_ON aliases
#define LED_ON_TRIS                 TRISDbits.TRISD6
#define LED_ON_LAT                  LATDbits.LATD6
#define LED_ON_PORT                 PORTDbits.RD6
#define LED_ON_WPU                  WPUDbits.WPUD6
#define LED_ON_OD                   ODCONDbits.ODCD6
#define LED_ON_ANS                  ANSELDbits.ANSELD6
#define LED_ON_SetHigh()            do { LATDbits.LATD6 = 1; } while(0)
#define LED_ON_SetLow()             do { LATDbits.LATD6 = 0; } while(0)
#define LED_ON_Toggle()             do { LATDbits.LATD6 = ~LATDbits.LATD6; } while(0)
#define LED_ON_GetValue()           PORTDbits.RD6
#define LED_ON_SetDigitalInput()    do { TRISDbits.TRISD6 = 1; } while(0)
#define LED_ON_SetDigitalOutput()   do { TRISDbits.TRISD6 = 0; } while(0)
#define LED_ON_SetPullup()          do { WPUDbits.WPUD6 = 1; } while(0)
#define LED_ON_ResetPullup()        do { WPUDbits.WPUD6 = 0; } while(0)
#define LED_ON_SetPushPull()        do { ODCONDbits.ODCD6 = 0; } while(0)
#define LED_ON_SetOpenDrain()       do { ODCONDbits.ODCD6 = 1; } while(0)
#define LED_ON_SetAnalogMode()      do { ANSELDbits.ANSELD6 = 1; } while(0)
#define LED_ON_SetDigitalMode()     do { ANSELDbits.ANSELD6 = 0; } while(0)

// get/set En_VS aliases
#define En_VS_TRIS                 TRISDbits.TRISD7
#define En_VS_LAT                  LATDbits.LATD7
#define En_VS_PORT                 PORTDbits.RD7
#define En_VS_WPU                  WPUDbits.WPUD7
#define En_VS_OD                   ODCONDbits.ODCD7
#define En_VS_ANS                  ANSELDbits.ANSELD7
#define En_VS_SetHigh()            do { LATDbits.LATD7 = 1; } while(0)
#define En_VS_SetLow()             do { LATDbits.LATD7 = 0; } while(0)
#define En_VS_Toggle()             do { LATDbits.LATD7 = ~LATDbits.LATD7; } while(0)
#define En_VS_GetValue()           PORTDbits.RD7
#define En_VS_SetDigitalInput()    do { TRISDbits.TRISD7 = 1; } while(0)
#define En_VS_SetDigitalOutput()   do { TRISDbits.TRISD7 = 0; } while(0)
#define En_VS_SetPullup()          do { WPUDbits.WPUD7 = 1; } while(0)
#define En_VS_ResetPullup()        do { WPUDbits.WPUD7 = 0; } while(0)
#define En_VS_SetPushPull()        do { ODCONDbits.ODCD7 = 0; } while(0)
#define En_VS_SetOpenDrain()       do { ODCONDbits.ODCD7 = 1; } while(0)
#define En_VS_SetAnalogMode()      do { ANSELDbits.ANSELD7 = 1; } while(0)
#define En_VS_SetDigitalMode()     do { ANSELDbits.ANSELD7 = 0; } while(0)

// get/set DE_M aliases
#define DE_M_TRIS                 TRISEbits.TRISE0
#define DE_M_LAT                  LATEbits.LATE0
#define DE_M_PORT                 PORTEbits.RE0
#define DE_M_WPU                  WPUEbits.WPUE0
#define DE_M_OD                   ODCONEbits.ODCE0
#define DE_M_ANS                  ANSELEbits.ANSELE0
#define DE_M_SetHigh()            do { LATEbits.LATE0 = 1; } while(0)
#define DE_M_SetLow()             do { LATEbits.LATE0 = 0; } while(0)
#define DE_M_Toggle()             do { LATEbits.LATE0 = ~LATEbits.LATE0; } while(0)
#define DE_M_GetValue()           PORTEbits.RE0
#define DE_M_SetDigitalInput()    do { TRISEbits.TRISE0 = 1; } while(0)
#define DE_M_SetDigitalOutput()   do { TRISEbits.TRISE0 = 0; } while(0)
#define DE_M_SetPullup()          do { WPUEbits.WPUE0 = 1; } while(0)
#define DE_M_ResetPullup()        do { WPUEbits.WPUE0 = 0; } while(0)
#define DE_M_SetPushPull()        do { ODCONEbits.ODCE0 = 0; } while(0)
#define DE_M_SetOpenDrain()       do { ODCONEbits.ODCE0 = 1; } while(0)
#define DE_M_SetAnalogMode()      do { ANSELEbits.ANSELE0 = 1; } while(0)
#define DE_M_SetDigitalMode()     do { ANSELEbits.ANSELE0 = 0; } while(0)

// get/set ZCB_ID aliases
#define ZCB_ID_TRIS                 TRISEbits.TRISE1
#define ZCB_ID_LAT                  LATEbits.LATE1
#define ZCB_ID_PORT                 PORTEbits.RE1
#define ZCB_ID_WPU                  WPUEbits.WPUE1
#define ZCB_ID_OD                   ODCONEbits.ODCE1
#define ZCB_ID_ANS                  ANSELEbits.ANSELE1
#define ZCB_ID_SetHigh()            do { LATEbits.LATE1 = 1; } while(0)
#define ZCB_ID_SetLow()             do { LATEbits.LATE1 = 0; } while(0)
#define ZCB_ID_Toggle()             do { LATEbits.LATE1 = ~LATEbits.LATE1; } while(0)
#define ZCB_ID_GetValue()           PORTEbits.RE1
#define ZCB_ID_SetDigitalInput()    do { TRISEbits.TRISE1 = 1; } while(0)
#define ZCB_ID_SetDigitalOutput()   do { TRISEbits.TRISE1 = 0; } while(0)
#define ZCB_ID_SetPullup()          do { WPUEbits.WPUE1 = 1; } while(0)
#define ZCB_ID_ResetPullup()        do { WPUEbits.WPUE1 = 0; } while(0)
#define ZCB_ID_SetPushPull()        do { ODCONEbits.ODCE1 = 0; } while(0)
#define ZCB_ID_SetOpenDrain()       do { ODCONEbits.ODCE1 = 1; } while(0)
#define ZCB_ID_SetAnalogMode()      do { ANSELEbits.ANSELE1 = 1; } while(0)
#define ZCB_ID_SetDigitalMode()     do { ANSELEbits.ANSELE1 = 0; } while(0)

// get/set Switch_PB aliases
#define Switch_PB_TRIS                 TRISEbits.TRISE2
#define Switch_PB_LAT                  LATEbits.LATE2
#define Switch_PB_PORT                 PORTEbits.RE2
#define Switch_PB_WPU                  WPUEbits.WPUE2
#define Switch_PB_OD                   ODCONEbits.ODCE2
#define Switch_PB_ANS                  ANSELEbits.ANSELE2
#define Switch_PB_SetHigh()            do { LATEbits.LATE2 = 1; } while(0)
#define Switch_PB_SetLow()             do { LATEbits.LATE2 = 0; } while(0)
#define Switch_PB_Toggle()             do { LATEbits.LATE2 = ~LATEbits.LATE2; } while(0)
#define Switch_PB_GetValue()           PORTEbits.RE2
#define Switch_PB_SetDigitalInput()    do { TRISEbits.TRISE2 = 1; } while(0)
#define Switch_PB_SetDigitalOutput()   do { TRISEbits.TRISE2 = 0; } while(0)
#define Switch_PB_SetPullup()          do { WPUEbits.WPUE2 = 1; } while(0)
#define Switch_PB_ResetPullup()        do { WPUEbits.WPUE2 = 0; } while(0)
#define Switch_PB_SetPushPull()        do { ODCONEbits.ODCE2 = 0; } while(0)
#define Switch_PB_SetOpenDrain()       do { ODCONEbits.ODCE2 = 1; } while(0)
#define Switch_PB_SetAnalogMode()      do { ANSELEbits.ANSELE2 = 1; } while(0)
#define Switch_PB_SetDigitalMode()     do { ANSELEbits.ANSELE2 = 0; } while(0)

/**
   @Param
    none
   @Returns
    none
   @Description
    GPIO and peripheral I/O initialization
   @Example
    PIN_MANAGER_Initialize();
 */
void PIN_MANAGER_Initialize (void);

/**
 * @Param
    none
 * @Returns
    none
 * @Description
    Interrupt on Change Handling routine
 * @Example
    PIN_MANAGER_IOC();
 */
void PIN_MANAGER_IOC(void);



#endif // PIN_MANAGER_H
/**
 End of File
*/