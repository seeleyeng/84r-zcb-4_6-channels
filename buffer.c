#include "buffer.h"
#include "global.h"

#define TIMEOUT_LONG    5
#define TIMEOUT_SHORT   1

void Buffer_new(Buffer * buffer)
{
    buffer->len = 0;
    buffer->crc = 0xFFFF;
    buffer->tick = TIMEOUT_LONG;
}

void Buffer_addData(Buffer * buffer, uint8_t _data)
{
    buffer->data[buffer->len] = _data;
    buffer->tick = TIMEOUT_SHORT;

    // include previous 2 data in CRC calculations
    if(buffer->len >= 2)
    {
        Buffer_updateCRC(buffer, buffer->data[buffer->len-2]);
    }
    buffer->len += 1;
}

uint8_t Buffer_getData(Buffer * buffer, uint8_t pos)
{
    if(pos < buffer->len)
        return buffer->data[pos];
    else
        return 0xFF;
}

uint8_t Buffer_getLen(Buffer * buffer)
{
    return buffer->len;
}

void Buffer_reset(Buffer * buffer)
{
    buffer->len = 0;
    buffer->crc = 0xFFFF;
    buffer->tick = TIMEOUT_LONG;
}

//----------CRC

void Buffer_updateCRC(Buffer* buffer, uint8_t data)
{
    buffer->crc ^= (uint16_t)data;

    for(uint8_t i = 8; i != 0; i--)
    {
        if((buffer-> crc & 0x0001) != 0)
        {
            buffer->crc >>= 1;
            buffer->crc ^= 0xA001;
        }
        else
        {
            buffer->crc >>= 1;
        }
    }
}


uint8_t Buffer_getCRCH(Buffer * buffer)
{
    uint8_t result = (uint8_t)(buffer->crc >> 8);
    return result;
}

uint8_t Buffer_getCRCL(Buffer * buffer)
{
    uint8_t result = (uint8_t)(buffer->crc & 0x00FF);
    return result;
}

uint8_t Buffer_checkCRC(Buffer * buffer)
{
    
    uint8_t crc_h = (uint8_t)(buffer->crc >> 8);
    uint8_t crc_l = (uint8_t)(buffer->crc & 0x00FF);
    
    if(buffer->data[buffer->len-1] == crc_h 
            && buffer->data[buffer->len-2] == crc_l)
        return TRUE;
    else
        return FALSE;
}

void Buffer_addCRC(Buffer * buffer)
{
    // include last 2 bytes into CRC
    Buffer_updateCRC(buffer, buffer->data[buffer->len-2]);
    Buffer_updateCRC(buffer, buffer->data[buffer->len-1]);

    // include crc to the tail
    buffer->data[buffer->len] = (uint8_t)(buffer->crc & 0x00FF);
    buffer->len++;
    buffer->data[buffer->len] = (uint8_t)(buffer->crc >> 8);
    buffer->len++;
}

void Buffer_txMain(Buffer * buffer)
{
    PIE3bits.TX2IE = 0;
    DE_Main = 1;
    __delay_us(100);
    for(uint8_t i = 0; i < buffer->len; i++)
    {
        TX2REG = buffer->data[i];
        PIE3bits.TX2IE = 1;
        while(PIE3bits.TX2IE == 1){}
    }
    PIE3bits.TX2IE = 0;
    __delay_us(1000);
    __delay_us(1000);
    DE_Main = 0;
}

void Buffer_txSub(Buffer * buffer, uint8_t de_mask)
{
    DE_COM = de_mask;
    PIE3bits.TX1IE = 0;
    __delay_us(100);
    for(uint8_t i = 0; i < buffer->len; i++)
    {
        TX1REG = buffer->data[i];
        PIE3bits.TX1IE = 1;
        while(PIE3bits.TX1IE == 1){}
    }
    PIE3bits.TX1IE = 0;
    __delay_us(1000);
    __delay_us(1000);
    DE_COM = 0;
}

void Buffer_rxMain(Buffer * buffer)
{
    DE_Main = 0;
    while(eusart2RxCount > 0)
    {
        Buffer_addData(buffer, EUSART2_Read());
    }
}

void Buffer_rxSub(Buffer * buffer)
{
    DE_COM = 0;
    while(eusart1RxCount > 0)
    {
        Buffer_addData(buffer, EUSART1_Read());
    }
}

void Buffer_tick(Buffer * buffer)
{
    if(buffer->tick > 0) buffer->tick--;
}

uint8_t Buffer_getState(Buffer * buffer)
{
    uint8_t state = IDLE;
    if(buffer->tick == 0) {state = DONE;}
    else if(buffer->len == 0)   {state = IDLE;}
    else {state = RUN;}
    return state;
}