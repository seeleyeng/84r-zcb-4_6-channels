#include "modbus.h"
#include "modbus_main.h"

// ********************//
// *** LOCAL MACROS ***//
// ********************//

// STATES for modbus main_modbus_states and sub_States
#define TRANSMIT        0x20
#define RECEIVE         0x21
#define NEXT            0x22
#define REDIRECT        0x23

// STATES for sub_message_states
#define RESET_ALL_SWC   0x30
#define SERVE_ZONE1     0x31
#define SERVE_ZONE2     0x32
#define SERVE_ZONE3     0x33
#define SERVE_ZONE4     0x34
#define SERVE_ZONE5     0x35
#define SERVE_ZONE6     0x36

#define TIMEOUT_LOST    100 //10s window to confirm lost connect with main modbus
#define RESET_ATTEMPT   20   //5 passes to send messages to reset all zone wc

// ************************//
// *** LOCAL VARIABLES  ***//
// ************************//

uint8_t main_modbus_state = RECEIVE;
uint8_t main_connect = FALSE;
uint8_t main_connect_cnt = 0;

uint8_t sub_modbus_state = TRANSMIT;
uint8_t sub_message_state = RESET_ALL_SWC;
uint8_t sub_message_state_saved = SERVE_ZONE2;

uint8_t redirect_state = IDLE;
uint8_t de_redirect = 0x00;

uint8_t reset_pass = 0;
uint8_t reset_swc = MODBUS_ID_SWC0;

// *************************//
// *** LOCAL PROTOTYPES  ***//
// *************************//

void main_modbus_update(void);
void sub_modbus_update(void);
void sub_message_tx(void);
void sub_message_rx(void);


// *************************//
// *** PUBLIC FUNCTIONS  ***//
// *************************//

// INITIALIZE all variables
void MODBUS_Initialize(void)
{
    DE_Main = 0;
    main_modbus_state = RECEIVE;
    main_connect = FALSE;
    main_connect_cnt = 0;
    
    sub_modbus_state = TRANSMIT;
    sub_message_state = RESET_ALL_SWC;

    
    de_redirect = 0x00;
    redirect_state = IDLE;
    
    reset_pass = 0;
    reset_swc = MODBUS_ID_SWC0;
}

// *************************//
// *** PUBLIC FUNCTIONS  ***//
// *************************//
// UPDATE main and sub modbus communication
void MODBUS_Update(void)
{
    main_modbus_update();
    sub_modbus_update();
}

// *************************//
// *** LOCAL FUNCTIONS  ***//
// *************************//
// UPDATE main modbus communication
// --- receive to message from master controller, store in buffer &main_rx
// --- response to message from master controller, update buffer &main_tx
// --- set redirect REQUEST, reset redirect to IDLE once redirect is completed
// --- track connection with master controller
void main_modbus_update(void)
{
    switch(main_modbus_state)
    {
        case RECEIVE: 
            Buffer_rxMain(&main_rx);
            if(Buffer_getState(&main_rx) == DONE)
            {
                if(Buffer_checkCRC(&main_rx) == TRUE)
                {
                    main_modbus_state = TRANSMIT;
                    break;
                }
                else
                {
                    Buffer_reset(&main_rx);
                }
            }

        break;
     
        case TRANSMIT: 
            Buffer_reset(&main_tx);
            main_connect_cnt = TIMEOUT_LOST;
            if(Buffer_getData(&main_rx, 0) == ZCB_getID(&zcb1) 
                    && (ZCB_type == ZCB_TYPE1))
            {
                ZCB_reply(&zcb1, &main_rx, &main_tx);
                Buffer_txMain(&main_tx);
                main_modbus_state = NEXT;
            }
            else if(Buffer_getData(&main_rx, 0) == ZCB_getID(&zcb2) 
                    && (ZCB_type == ZCB_TYPE2))
            {
                ZCB_reply(&zcb2, &main_rx, &main_tx);
                Buffer_txMain(&main_tx);
                main_modbus_state = NEXT;
            }
            else if(Buffer_getData(&main_rx, 0) == Channel_getID(&channel1))
            {
                de_redirect = ZONE_1;
                redirect_state = REQUEST;
                main_modbus_state = REDIRECT;
            }
            else if(Buffer_getData(&main_rx, 0) == Channel_getID(&channel2))
            {
                de_redirect = ZONE_2;
                redirect_state = REQUEST;
                main_modbus_state = REDIRECT;
            }
            else if(Buffer_getData(&main_rx, 0) == Channel_getID(&channel3))
            {
                de_redirect = ZONE_3;
                redirect_state = REQUEST;
                main_modbus_state = REDIRECT;
            }
            else if(Buffer_getData(&main_rx, 0) == Channel_getID(&channel4))
            {
                de_redirect = ZONE_4;
                redirect_state = REQUEST;
                main_modbus_state = REDIRECT;
            }
            else if(Buffer_getData(&main_rx, 0) == Channel_getID(&channel5))
            {
                de_redirect = ZONE_5;
                redirect_state = REQUEST;
                main_modbus_state = REDIRECT;
            }
            else if(Buffer_getData(&main_rx, 0) == Channel_getID(&channel6))
            {
                de_redirect = ZONE_6;
                redirect_state = REQUEST;
                main_modbus_state = REDIRECT;
            }
            else
            {
                main_modbus_state = NEXT;
            }
        break;
 
        case REDIRECT: 
            if(redirect_state == DONE)
            {
                redirect_state = IDLE;
                main_modbus_state = NEXT;
            }
            break;
        
        case NEXT:
            Buffer_reset(&main_rx);
            Buffer_reset(&main_tx);
            main_modbus_state = RECEIVE;
            break;
    }
    
    if(main_connect_cnt == 0) main_connect = FALSE;
    else main_connect = TRUE;

}

void sub_modbus_update(void)
{
    switch(sub_modbus_state)
    {
        case TRANSMIT: 
            Buffer_reset(&sub_tx);
            Buffer_reset(&sub_rx);

            if(redirect_state == IDLE)
            {
                sub_message_tx();
                sub_modbus_state = RECEIVE;
            }
            else if(redirect_state == REQUEST)
            {
                redirect_state = RUN;
                Buffer_txSub(&main_rx, de_redirect);
                sub_modbus_state = REDIRECT;
            }
            break;
            
        case RECEIVE:
            Buffer_rxSub(&sub_rx);
            
            if(Buffer_getState(&sub_rx) == DONE)
            {    
                sub_message_rx();
                sub_modbus_state = TRANSMIT;
            }
            break;
            
        case REDIRECT: 
            Buffer_rxSub(&sub_rx);
            if(Buffer_getState(&sub_rx) == DONE)
            {
                Buffer_txMain(&sub_rx);
                redirect_state = DONE;
                sub_modbus_state = TRANSMIT;
            }
            
            break;
    }
}

void sub_message_tx(void)
{
    switch(sub_message_state)
    {
        case RESET_ALL_SWC: 
            Buffer_addData(&sub_tx, reset_swc);
            Buffer_addData(&sub_tx, WR_FUNCTION);
            Buffer_addData(&sub_tx, (uint8_t)(R_ID_SWC >> 8));
            Buffer_addData(&sub_tx, (uint8_t)(R_ID_SWC & 0x00FF));
            Buffer_addData(&sub_tx, 0);
            Buffer_addData(&sub_tx, 1);
            Buffer_addData(&sub_tx, 2);
            Buffer_addData(&sub_tx, 0);
            Buffer_addData(&sub_tx, MODBUS_ID_SWC0);
            Buffer_addCRC(&sub_tx);
            Buffer_txSub(&sub_tx, ZONE_ALL);
            break;
        case SERVE_ZONE1: 
            Channel_setMessage(&channel1, &sub_tx);
            Buffer_txSub(&sub_tx, ZONE_1);
            break;
        case SERVE_ZONE2: 
            Channel_setMessage(&channel2, &sub_tx);
            Buffer_txSub(&sub_tx, ZONE_2);
            break;
        case SERVE_ZONE3 : 
            Channel_setMessage(&channel3, &sub_tx);
            Buffer_txSub(&sub_tx, ZONE_3);
            break;
        case SERVE_ZONE4: 
            Channel_setMessage(&channel4, &sub_tx);
            Buffer_txSub(&sub_tx, ZONE_4);
            break;
        case SERVE_ZONE5: 
            Channel_setMessage(&channel5, &sub_tx);
            Buffer_txSub(&sub_tx, ZONE_5);
            break;
        case SERVE_ZONE6: 
            Channel_setMessage(&channel6, &sub_tx);
            Buffer_txSub(&sub_tx, ZONE_6);
            break;
        case IDLE:
            break;
    }

}

void sub_message_rx(void)
{
    switch(sub_message_state)
    {
        case RESET_ALL_SWC: 
            if(reset_swc < MODBUS_ID_SWC9)
            {
                reset_swc++;
            }
            else
            {
                reset_pass++;
                reset_swc = MODBUS_ID_SWC0;
                if(reset_pass > RESET_ATTEMPT)
                {
                    if(ZCB_type == ZCB_TYPE1)
                    {
                        sub_message_state = SERVE_ZONE2;
                    }
                    else
                    {
                        sub_message_state = SERVE_ZONE1;
                    }
                }
            }
            break;
        case SERVE_ZONE1:
            Channel_process(&channel1, &sub_rx);
            sub_message_state = SERVE_ZONE2;
            break;
        case SERVE_ZONE2:
            Channel_process(&channel2, &sub_rx);
            sub_message_state = SERVE_ZONE3;
            break;
        case SERVE_ZONE3: 
            Channel_process(&channel3, &sub_rx);
            sub_message_state = SERVE_ZONE4;
            break;
        case SERVE_ZONE4: 
            Channel_process(&channel4, &sub_rx);
            if(ZCB_type == ZCB_TYPE1)
            {
                sub_message_state = SERVE_ZONE2;
            }
            else
            {
                sub_message_state = SERVE_ZONE5;
            }
            break;
        case SERVE_ZONE5: 
            Channel_process(&channel5, &sub_rx);
            sub_message_state = SERVE_ZONE6;
            break;
        case SERVE_ZONE6: 
            Channel_process(&channel6, &sub_rx);
            sub_message_state = SERVE_ZONE1;
            break;
        case IDLE:
            break;

    }

}
