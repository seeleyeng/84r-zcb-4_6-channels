#include "channel.h"
#include "global.h"
#include "modbus_main.h"
#include "modbus.h"

void Channel_new(Channel * channel, uint8_t _mask, uint8_t _swc_expected, uint8_t _as_expected)
{
    channel->mask = _mask;

    channel->controller_connect = MODBUS_ID_NULL;
    channel->controller_status = CONTROL_SWC_FIND;
    channel->controller_lost_cnt = 0;


    channel->swc_expected = _swc_expected;
    channel->swc_find = MODBUS_ID_SWC0;

    channel->as_expected = _as_expected;
    channel->as_find = MODBUS_ID_AS0;
    
    channel->sw_state_on = FALSE;
    channel->sw_state_cnt = 0;
    
    channel->damper_state_energised = FALSE;
    channel->damper_state_cnt = 0;
    
}

void Channel_setMessage(Channel * channel, Buffer * message_tx)
{
    Buffer_reset(message_tx);
    switch(channel->controller_status)
    {
        case CONTROL_SWC_FIND: 
            Buffer_addData(message_tx, channel->swc_find);
            Buffer_addData(message_tx, WR_FUNCTION);
            Buffer_addData(message_tx, (uint8_t)(R_ID_SWC >> 8));
            Buffer_addData(message_tx, (uint8_t)(R_ID_SWC & 0x00FF));
            Buffer_addData(message_tx, 0);
            Buffer_addData(message_tx, 1);
            Buffer_addData(message_tx, 2);
            Buffer_addData(message_tx, 0);
            Buffer_addData(message_tx, channel->swc_expected);
            Buffer_addCRC(message_tx);
            break;
            
        case CONTROL_SWC_TRACK:
            Buffer_addData(message_tx, channel->swc_expected);
            Buffer_addData(message_tx, RD_FUNCTION);
            Buffer_addData(message_tx, (uint8_t)(R_ID_SWC >> 8));
            Buffer_addData(message_tx, (uint8_t)(R_ID_SWC & 0x00FF));
            Buffer_addData(message_tx, 0);
            Buffer_addData(message_tx, 1);
            Buffer_addCRC(message_tx);
            break;

        case CONTROL_AS_FIND: 
            Buffer_addData(message_tx, channel->as_find);
            Buffer_addData(message_tx, RD_FUNCTION);
            Buffer_addData(message_tx, (uint8_t)(R_ID_AS >> 8));
            Buffer_addData(message_tx, (uint8_t)(R_ID_AS & 0x00FF));
            Buffer_addData(message_tx, 0);
            Buffer_addData(message_tx, 1);
            Buffer_addCRC(message_tx);
            break;
            
        case CONTROL_AS_TRACK: 
            Buffer_addData(message_tx, channel->as_expected);
            Buffer_addData(message_tx, RD_FUNCTION);
            Buffer_addData(message_tx, (uint8_t)(R_ID_AS >> 8));
            Buffer_addData(message_tx, (uint8_t)(R_ID_AS & 0x00FF));
            Buffer_addData(message_tx, 0);
            Buffer_addData(message_tx, 1);
            Buffer_addCRC(message_tx);
            break;

        case CONTROL_AS_RESET: 
            Buffer_addData(message_tx, channel->controller_connect);
            Buffer_addData(message_tx, WR_FUNCTION);
            Buffer_addData(message_tx, (uint8_t)(R_ID_AS >> 8));
            Buffer_addData(message_tx, (uint8_t)(R_ID_AS & 0x00FF));
            Buffer_addData(message_tx, 0);
            Buffer_addData(message_tx, 1);
            Buffer_addData(message_tx, 2);
            Buffer_addData(message_tx, 0);
            Buffer_addData(message_tx, channel->as_expected);
            Buffer_addCRC(message_tx);
            break;    
    }
}

void Channel_process(Channel * channel, Buffer * message_rx)
{
    uint8_t message_valid;              // replied message check result
    
    message_valid = TRUE;
    if(Buffer_getLen(message_rx) < 6) message_valid = FALSE;
    if(Buffer_checkCRC(message_rx) == FALSE) message_valid = FALSE;
    
    switch(channel->controller_status)
    {
        case CONTROL_SWC_FIND: 
            if(message_valid == TRUE)
            {
                if(Buffer_getData(message_rx, 0) == channel->swc_expected)
                {
                    // found wall control and set to expected id
                    Channel_setID(channel, channel->swc_expected);
                }
            }
            if(Channel_getID(channel) == channel->swc_expected)
            {
                channel->controller_status = CONTROL_SWC_TRACK;
                break;
            }
            else
            {   // cannot find any wall control
                // target next swc, restart if out of range
                channel->swc_find++; 
                if(channel->swc_find > MODBUS_ID_SWC9)
                {
                    channel->swc_find = MODBUS_ID_SWC0;
                }
                // find air sensor and swc alternatively
                channel->controller_status = CONTROL_AS_FIND;
            }
            break;
            
        case CONTROL_SWC_TRACK:
            if(Buffer_getData(message_rx, 0) != Channel_getID(channel)) message_valid = FALSE;
            if(Buffer_getData(message_rx, 1) != RD_FUNCTION) message_valid = FALSE;
            if(message_valid == FALSE)
            { // no / invalid reply
                Channel_lostID(channel);  //will reset ID after 5 attempts
            }
            else
            {
                Channel_setID(channel, channel->swc_expected);
            }
            if(Channel_getID(channel) != channel->swc_expected)
            {
                channel->swc_find = channel->swc_expected;
                channel->controller_status = CONTROL_SWC_FIND;
            }
            break;
        
        case CONTROL_AS_FIND: 
            if(Buffer_getData(message_rx, 1) != RD_FUNCTION) message_valid = FALSE;
            if(Buffer_getData(message_rx, 0) < MODBUS_ID_AS0 
                    || Buffer_getData(message_rx, 0) > MODBUS_ID_AS9)
                message_valid = FALSE;
            if(message_valid == TRUE)
            {
                if(Buffer_getData(message_rx, 0) == channel->as_expected)
                {
                    // found wall control and set to expected id
                    Channel_setID(channel, channel->as_expected);
                    channel->controller_status = CONTROL_AS_TRACK;
                }
                else
                {
                    Channel_setID(channel, Buffer_getData(message_rx, 0));
                    channel->controller_status = CONTROL_AS_RESET;
                }
            }
            if(message_valid == FALSE)
            {   // cannot find any air sensor
                // target next as, restart if out of range
                channel->as_find++; 
                if(channel->as_find > MODBUS_ID_AS9)
                {
                    channel->as_find = MODBUS_ID_AS0;
                }
                // find air sensor and swc alternatively
                channel->controller_status = CONTROL_SWC_FIND;
            }
            break;
        
        case CONTROL_AS_TRACK:
            // *** check message
            if(Buffer_getData(message_rx, 0) != channel->as_expected) message_valid = FALSE;
            if(Buffer_getData(message_rx, 1) != RD_FUNCTION) message_valid = FALSE;

            // *** response to checked message
            if(message_valid == FALSE)
            { // --- no / invalid reply
                Channel_lostID(channel);  //will reset ID after 5 attempts
            }
            else
            { // --- receive valid reply
                //confirm the channel is set and reset lost count
                Channel_setID(channel, channel->as_expected); 
            }
            // *** change state base on connection status
            if(Channel_getID(channel) != channel->as_expected)
            {
                channel->as_find = channel->as_expected;
                channel->controller_status = CONTROL_AS_FIND;
            }            
            break;
            
        case CONTROL_AS_RESET: 
          //  Channel_setID(channel, channel->as_expected);
            channel->as_find = channel->as_expected;
            channel->controller_status = CONTROL_AS_FIND;
            break;    
    }
    
}

void Channel_setID(Channel * channel, uint8_t id_connect)
{
    channel->controller_connect = id_connect;
    channel->controller_lost_cnt = 0;
}

void Channel_lostID(Channel * channel)
{
    if(channel->controller_lost_cnt > CONTROL_LOST)
    {
        channel->controller_lost_cnt = 0;
        channel->controller_connect = MODBUS_ID_NULL;
    }
    else
    {
        channel->controller_lost_cnt++;
    }
}

uint8_t Channel_getID(Channel * channel)
{
    return channel->controller_connect;
}

uint8_t Channel_updateSw(Channel * channel, uint8_t input)
{  
    switch(channel->sw_state_on)
    {
        case TRUE: 
            if(input & channel->mask)
            {
                if(channel->sw_state_cnt > SW_DEBOUNCE)
                    channel->sw_state_on = FALSE;
            }
            else
            {
                channel-> sw_state_cnt = 0;
            }
            break;
            
        case FALSE: 
            if(input & channel->mask)
            {
                channel-> sw_state_cnt = 0;
            }
            else
            {
                if(channel->sw_state_cnt > SW_DEBOUNCE)
                    channel->sw_state_on = TRUE;
            }
            break;
        default: 
            channel->sw_state_on = FALSE;
            break;
    }
    
    if(channel->sw_state_on == TRUE) return channel->mask;
    else return 0;
}

uint8_t Channel_updateDamper(Channel * channel, uint8_t input)
{
    switch(channel->damper_state_energised)
    {
        case TRUE: 
            if(input & channel->mask)
            {
                
            }
            else
            {
                channel->damper_state_energised = FALSE;
            }
            break;
        case FALSE: 
            if(input & channel->mask)
            {
                channel->damper_state_energised = TRUE;
            }
            else
            {
            }
            break;
        default:
            channel->sw_state_on = FALSE;
            break;
    }
    
    return channel->damper_state_energised;
}

void Channel_tickSw(Channel * channel)
{
    (channel-> sw_state_cnt)++;
}

void Channel_tickDamper(Channel * channel)
{
    (channel->damper_state_cnt)++;
}
