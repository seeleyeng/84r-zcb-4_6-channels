/**
  Generated Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This is the main file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  Description:
    This header file provides implementations for driver APIs for all modules selected in the GUI.
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.81.7
        Device            :  PIC18F47Q10
        Driver Version    :  2.00
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/
#include "mcc_generated_files/mcc.h"
#include "modbus_main.h"
#include "modbus.h"

#include "channel.h"
#include "zcb.h"
#include "global.h"


#define TICK_200MS 200

#define MASK_LEDON 0b01000000   // power led
#define MASK_DEBUG 0b10000000   // debugging led



//-------------PARAMETERS-------------------------------------------
//-------------PARAMETERS-------------------------------------------
//-------------PARAMETERS-------------------------------------------
//-------------PARAMETERS-------------------------------------------
//-------------PARAMETERS-------------------------------------------
uint8_t ZCB1_connect = 0;
uint8_t ZCB1_dampers = 0;
uint8_t ZCB1_switches = 0;

uint8_t ZCB2_connect = 0;
uint8_t ZCB2_dampers = 0;
uint8_t ZCB2_switches = 0;

uint8_t ZCB3_connect = 0;
uint8_t ZCB3_dampers = 0;
uint8_t ZCB3_switches = 0;

uint8_t MAP_led = 0;
uint8_t MAP_dampers = 0;
uint8_t MAP_dampers_out = 0;
uint8_t MAP_switches = 0;


//-------------PROTOTYPE---------------------------------------------
//-------------PROTOTYPE---------------------------------------------
//-------------PROTOTYPE---------------------------------------------
//-------------PROTOTYPE---------------------------------------------
//-------------PROTOTYPE---------------------------------------------
void MAIN_Initialize(void);
void MAIN_Update(void);

// -------------MAIN APPLICATION--------------------------------------
// -------------MAIN APPLICATION--------------------------------------
// -------------MAIN APPLICATION--------------------------------------
// -------------MAIN APPLICATION--------------------------------------
// -------------MAIN APPLICATION--------------------------------------
// -------------MAIN APPLICATION--------------------------------------
void main(void)
{
    
    // Initialize the device
    SYSTEM_Initialize();
    MAIN_Initialize();
    MODBUS_Initialize();
    
    // Enable the Global Interrupts
    INTERRUPT_GlobalInterruptEnable();
    INTERRUPT_PeripheralInterruptEnable();
    
    while (1)
    {
        MODBUS_Update();
        MAIN_Update();
        CLRWDT(); 
    }
}

void MAIN_Initialize(void)
{
    // disable all interrupts
    INTERRUPT_GlobalInterruptDisable();
    INTERRUPT_PeripheralInterruptDisable();
    
    // define zone control board type when the reading is stable for 250ms
    uint8_t zcb_type_cnt = 0;
    ZCB_type = ZCB_TYPE1;
    while(zcb_type_cnt < 250)
    {
        if(ZCB_type == ZCB_TYPE2)
        {
            if(PortE_in & MASK_ID)
            {
                zcb_type_cnt = 0;
                ZCB_type = ZCB_TYPE1;
            }
            else
            {
                zcb_type_cnt++;
            }
        }
        else
        {
            if(PortE_in & MASK_ID)
            {
                zcb_type_cnt++;
            }
            else
            {
                zcb_type_cnt = 0;
                ZCB_type = ZCB_TYPE2;
            }
        }
        __delay_us(1000);
    }

    // initialise zone and channels
    if(ZCB_type == ZCB_TYPE1)
    {
        // setup zcb
        ZCB_new(&zcb1, MODBUS_ID_ZCB1);

        // setup channels
        Channel_new(&channel1, ZONE_1, MODBUS_ID_NULL, MODBUS_ID_NULL);
        Channel_new(&channel2, ZONE_2, MODBUS_ID_SWC1, MODBUS_ID_AS1);
        Channel_new(&channel3, ZONE_3, MODBUS_ID_SWC2, MODBUS_ID_AS2);
        Channel_new(&channel4, ZONE_4, MODBUS_ID_SWC3, MODBUS_ID_AS3);
        
    }
    else
    {
        // setup zcbs
        ZCB_new(&zcb2, MODBUS_ID_ZCB2);
        ZCB_new(&zcb3, MODBUS_ID_ZCB3);

        // ignore modbus id 213 request until zcb3_active = true;
        zcb3_active = FALSE;
        
        // setup channels
        Channel_new(&channel1, ZONE_1, MODBUS_ID_SWC4, MODBUS_ID_AS4);
        Channel_new(&channel2, ZONE_2, MODBUS_ID_SWC5, MODBUS_ID_AS5);
        Channel_new(&channel3, ZONE_3, MODBUS_ID_SWC6, MODBUS_ID_AS6);
        Channel_new(&channel4, ZONE_4, MODBUS_ID_SWC7, MODBUS_ID_AS7);
        Channel_new(&channel5, ZONE_5, MODBUS_ID_SWC8, MODBUS_ID_AS8);
        Channel_new(&channel6, ZONE_6, MODBUS_ID_SWC9, MODBUS_ID_AS9);
    }
    
    // initialise communication buffers
    Buffer_new(&main_rx);
    Buffer_new(&main_tx);
    Buffer_new(&sub_rx);
    Buffer_new(&sub_tx);

    // initialise io mappings
    MAP_led = 0;
    MAP_dampers = 0;
    MAP_switches = 0;
    MAP_dampers_out = 0;

    // enable all interrupts
    INTERRUPT_GlobalInterruptEnable();
    INTERRUPT_PeripheralInterruptEnable();
}

void MAIN_Update(void)
{
    
    // update switches inputs
    MAP_switches = 0;
    MAP_switches |= Channel_updateSw(&channel1, PortB_in);
    MAP_switches |= Channel_updateSw(&channel2, PortB_in);
    MAP_switches |= Channel_updateSw(&channel3, PortB_in);
    MAP_switches |= Channel_updateSw(&channel4, PortB_in);
    MAP_switches |= Channel_updateSw(&channel5, PortB_in);
    MAP_switches |= Channel_updateSw(&channel6, PortB_in);
    
    
    if(ZCB_type == ZCB_TYPE1)
    {//ZCB 1

        // update dampers request
        ZCB1_dampers = (uint8_t) (ZCB_getReg(&zcb1, R40251) & 0x000F);
        MAP_dampers = ZCB1_dampers;

        // update controllers connected
        ZCB1_connect = 0;
        MAP_led = 0;
        // --- channel 2
        if(Channel_getID(&channel2) == MODBUS_ID_AS1)
        {
            ZCB1_connect |= 0x0C;
            MAP_led |= ZONE_2;
        }
        else if(Channel_getID(&channel2) == MODBUS_ID_SWC1)
        {
            ZCB1_connect |= 0x04;
            MAP_led |= ZONE_2;
        }

        // --- channel 3
        if(Channel_getID(&channel3) == MODBUS_ID_AS2)
        {
            ZCB1_connect |= 0x30;
            MAP_led |= ZONE_3;
        }
        else if(Channel_getID(&channel3) == MODBUS_ID_SWC2)
        {
            ZCB1_connect |= 0x10;
            MAP_led |= ZONE_3;
        }
        
        // --- channel 4
        if(Channel_getID(&channel4) == MODBUS_ID_AS3)
        {
            ZCB1_connect |= 0xC0;
            MAP_led |= ZONE_4;
        }
        else if(Channel_getID(&channel4) == MODBUS_ID_SWC3)
        {
            ZCB1_connect |= 0x40;
            MAP_led |= ZONE_4;
        }


        // update switches registers
        ZCB1_switches = (MAP_switches & ZCB_type);
        ZCB_setReg(&zcb1, R40252, (uint16_t) (ZCB1_switches << 8) | ZCB1_connect);

    }
    else
    {// ZCB2 and ZCB 3
        
        // update dampers requests
        ZCB2_dampers = (uint8_t) (ZCB_getReg(&zcb2, R40251) & 0x000F);
        ZCB2_dampers = (ZCB2_dampers & 0x0F) >> 1;
        
        ZCB3_dampers = (uint8_t) (ZCB_getReg(&zcb3, R40251) & 0x000F);
        ZCB3_dampers = (ZCB3_dampers & 0x0F) >> 1;
        
        MAP_dampers = (ZCB2_dampers) | (uint8_t)(ZCB3_dampers << 3);

        // update controllers connected
        ZCB2_connect = 0;
        ZCB3_connect = 0;
        MAP_led = 0;
        
        // --- channel 1
        if(Channel_getID(&channel1) == MODBUS_ID_AS4)
        {
            ZCB2_connect |= 0x0C;
            MAP_led |= ZONE_1;
        }
        else if(Channel_getID(&channel1) == MODBUS_ID_SWC4)
        {
            ZCB2_connect |= 0x04;
            MAP_led |= ZONE_1;
        }

        // --- channel 2
        if(Channel_getID(&channel2) == MODBUS_ID_AS5)
        {
            ZCB2_connect |= 0x30;
            MAP_led |= ZONE_2;
        }
        else if(Channel_getID(&channel2) == MODBUS_ID_SWC5)
        {
            ZCB2_connect |= 0x10;
            MAP_led |= ZONE_2;
        }

        // --- channel 3
        if(Channel_getID(&channel3) == MODBUS_ID_AS6)
        {
            ZCB2_connect |= 0xC0;
            MAP_led |= ZONE_3;
        }
        else if(Channel_getID(&channel3) == MODBUS_ID_SWC6)
        {
            ZCB2_connect |= 0x40;
            MAP_led |= ZONE_3;
        }
        
        // --- channel 4
        if(Channel_getID(&channel4) == MODBUS_ID_AS7)
        {
            ZCB3_connect |= 0x0C;
            MAP_led |= ZONE_4;
        }
        else if(Channel_getID(&channel4) == MODBUS_ID_SWC7)
        {
            ZCB3_connect |= 0x04;
            MAP_led |= ZONE_4;
        }

        // --- channel 5
        if(Channel_getID(&channel5) == MODBUS_ID_AS8)
        {
            ZCB3_connect |= 0x30;
            MAP_led |= ZONE_5;
        }
        else if(Channel_getID(&channel5) == MODBUS_ID_SWC8)
        {
            ZCB3_connect |= 0x10;
            MAP_led |= ZONE_5;
        }

        // --- channel 6
        if(Channel_getID(&channel6) == MODBUS_ID_AS9)
        {
            ZCB3_connect |= 0xC0;
            MAP_led |= ZONE_6;
        }
        else if(Channel_getID(&channel6) == MODBUS_ID_SWC9)
        {
            ZCB3_connect |= 0x40;
            MAP_led |= ZONE_6;
        }
        
        // update registers
        MAP_switches = (MAP_switches & ZCB_type);
        ZCB2_switches = MAP_switches & 0b00000111;
        ZCB2_switches = (uint8_t)(ZCB2_switches << 1);
        ZCB3_switches = MAP_switches & 0b00111000;
        ZCB3_switches = (uint8_t) (ZCB3_switches >> 2);

        // update zcb3 status base on damper switches inputs
        if(ZCB3_switches == 0)
        {
            zcb3_active = FALSE;
        }
        else
        {
            zcb3_active = TRUE;
        }
        
        //update switches registers
        ZCB_setReg(&zcb2, R40252, (uint16_t) (ZCB2_switches << 8) | ZCB2_connect);
        ZCB_setReg(&zcb3, R40252, (uint16_t) (ZCB3_switches << 8) | ZCB3_connect);

    }

    // update dampers drivers
    MAP_dampers_out = 0;
    if(Channel_updateDamper(&channel1, MAP_dampers) == TRUE)
    {
        MAP_dampers_out |= ZONE_1;
    }
    if(Channel_updateDamper(&channel2, MAP_dampers) == TRUE)
    {
        MAP_dampers_out |= ZONE_2;
    }
    if(Channel_updateDamper(&channel3, MAP_dampers) == TRUE)
    {
        MAP_dampers_out |= ZONE_3;
    }
    if(Channel_updateDamper(&channel4, MAP_dampers) == TRUE)
    {
        MAP_dampers_out |= ZONE_4;
    }
    if(Channel_updateDamper(&channel5, MAP_dampers) == TRUE)
    {
        MAP_dampers_out |= ZONE_5;
    }
    if(Channel_updateDamper(&channel6, MAP_dampers) == TRUE)
    {
        MAP_dampers_out |= ZONE_6;
    }

    PortA_out = (MAP_dampers_out & ZCB_type);   // damper output

}


/**
 End of File
*/