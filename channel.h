/* 
 * File:   channel.h
 * Author: rtiong
 *
 * Created on February 15, 2022, 10:24 AM
 */

#ifndef CHANNEL_H
#define	CHANNEL_H

#include <xc.h>
#include "buffer.h"

#define CONTROL_SWC_FIND    0x11
#define CONTROL_SWC_TRACK   0x12
#define CONTROL_AS_FIND     0x21  
#define CONTROL_AS_RESET    0x22    
#define CONTROL_AS_TRACK    0x23

#define CONTROL_LOST        1   //5 attempts before confirm controller lost
#define SW_DEBOUNCE         250 //250ms debounce between switch state change
#define DAMPER_DELAY        5   //5s delay between damper state change

typedef struct
{
    uint8_t mask;
    uint8_t controller_connect;
    
    uint8_t controller_status;
    uint8_t controller_lost_cnt;

    
    uint8_t swc_find;
    uint8_t swc_expected;

    uint8_t as_find;
    uint8_t as_expected;
    
    uint8_t sw_state_on;
    uint8_t sw_state_cnt;
    
    uint8_t damper_state_energised;
    uint8_t damper_req_energised;
    uint8_t damper_state_cnt;
    
} Channel;



void Channel_new(Channel * channel, uint8_t mask, uint8_t swc_expected, uint8_t as_expected);

uint8_t Channel_getID(Channel * channel);
void Channel_setID(Channel * channel, uint8_t id_connect);
void Channel_lostID(Channel * channel);

void Channel_setMessage(Channel * channel, Buffer * message_tx);
void Channel_process(Channel * channel, Buffer * message_rx);

uint8_t Channel_updateSw(Channel * channel, uint8_t input);
void Channel_tickSw(Channel * channel);

uint8_t Channel_updateDamper(Channel * channel, uint8_t input);
void Channel_tickDamper(Channel * channel);

#endif	/* CHANNEL_H */

