/* 
 * File:   buffer.h
 * Author: rtiong
 *
 * Created on March 8, 2022, 8:58 AM
 */

#ifndef BUFFER_H
#define	BUFFER_H

#include <xc.h>

#define BUFFER_SIZE 128



typedef struct
{
    uint8_t data[BUFFER_SIZE];
    uint8_t len;
    uint16_t crc;
    uint8_t tick;
}Buffer;


void Buffer_new(Buffer * buffer);
void Buffer_addData(Buffer * buffer, uint8_t _data);
void Buffer_reset(Buffer * buffer);
uint8_t Buffer_getLen(Buffer * buffer);
uint8_t Buffer_getData(Buffer * buffer, uint8_t pos);

void Buffer_updateCRC(Buffer* buffer, uint8_t data);
uint8_t Buffer_getCRCH(Buffer * buffer);
uint8_t Buffer_getCRCL(Buffer * buffer);
uint8_t Buffer_checkCRC(Buffer * buffer);
void Buffer_addCRC(Buffer * buffer);

void Buffer_txMain(Buffer * buffer);
void Buffer_txSub(Buffer * buffer, uint8_t de_mask);
void Buffer_rxMain(Buffer * buffer);
void Buffer_rxSub(Buffer * buffer);

uint8_t Buffer_getState(Buffer * buffer);
void Buffer_tick(Buffer * buffer);


#endif