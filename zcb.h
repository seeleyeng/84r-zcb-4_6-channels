/* 
 * File:   io.h
 * Author: rtiong
 *
 * Created on January 10, 2022, 2:42 PM
 */

#ifndef ZCB_H
#define	ZCB_H

#include "mcc_generated_files/mcc.h"
#include "buffer.h"
#include <xc.h>

#define ZCB_RLEN    6
#define ZCB_RBASE   249

typedef struct
{
    uint8_t id;
    uint16_t reg[ZCB_RLEN];
} ZCB;


void ZCB_new(ZCB * zcb, uint8_t _id);
uint8_t ZCB_getID(ZCB * zcb);

void ZCB_setReg(ZCB * zcb, uint16_t _reg, uint16_t value);
uint16_t ZCB_getReg(ZCB * zcb, uint16_t _reg);
void ZCB_reply(ZCB * zcb, Buffer * message_rx, Buffer * message_tx);

#endif

