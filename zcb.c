#include "zcb.h"
#include "global.h"
#include "modbus_main.h"

#define REQ_ZONE_RECONFIG   0x0100

//-----------MAIN----ROUTINES-------------------------------------------------
//-----------MAIN----ROUTINES-------------------------------------------------
//-----------MAIN----ROUTINES-------------------------------------------------
//-----------MAIN----ROUTINES-------------------------------------------------
//-----------MAIN----ROUTINES-------------------------------------------------

void ZCB_new(ZCB * zcb, uint8_t _id)
{
    zcb->id = _id;
    zcb->reg[R40250-ZCB_RBASE] = _id;
    zcb->reg[R40253-ZCB_RBASE] = SOFTWARE_TYPE;
    zcb->reg[R40254-ZCB_RBASE] = SOFTWARE_TYPE;
    zcb->reg[R40255-ZCB_RBASE] = SOFTWARE_TYPE;
}

uint8_t ZCB_getID(ZCB * zcb)
{
    return zcb->id;
}

void ZCB_setReg(ZCB * zcb, uint16_t reg, uint16_t value)
{
    zcb->reg[reg-ZCB_RBASE] = value;
}

uint16_t ZCB_getReg(ZCB * zcb, uint16_t reg)
{
    return zcb->reg[reg-ZCB_RBASE];
}

void ZCB_reply(ZCB * zcb, Buffer * message_rx, Buffer * message_tx)
{
    
    uint16_t sreg = (uint16_t)(Buffer_getData(message_rx, 2) << 8);
    sreg += (uint16_t)(Buffer_getData(message_rx, 3));
    
    uint16_t nreg = (uint16_t)(Buffer_getData(message_rx, 4) << 8);
    nreg += (uint16_t)(Buffer_getData(message_rx, 5));
    
    uint8_t nbyte = (uint8_t) (nreg << 1);
    
    //-----
    uint8_t preg = 0, pmsg = 7;
    uint16_t data;
    
    //-----
    
    Buffer_reset(message_tx);
    Buffer_addData(message_tx, zcb->id);
    
    switch(Buffer_getData(message_rx, 1))
    {
        case RD_FUNCTION: 
            Buffer_addData(message_tx, RD_FUNCTION);
            Buffer_addData(message_tx, nbyte);

            while(preg < nreg)
            {
                data = ZCB_getReg(zcb, sreg + preg); preg++;
                Buffer_addData(message_tx, (uint8_t)(data >> 8));
                Buffer_addData(message_tx, (uint8_t)(data & 0x00FF));
            }
            Buffer_addCRC(message_tx);
            Buffer_txMain(message_tx);
            
            break;

        case WR_FUNCTION: 
            while(preg < nreg)
            {
                data = (uint16_t)(Buffer_getData(message_rx, pmsg) << 8); pmsg++;
                data += (uint16_t)(Buffer_getData(message_rx, pmsg)); pmsg++;
                
                if((sreg + preg) == R40251)
                {
                    if(data & REQ_ZONE_RECONFIG)
                        data &= ~((uint16_t)REQ_ZONE_RECONFIG);
                }
                ZCB_setReg(zcb, sreg + preg, data);
                preg++;
            }
            Buffer_addData(message_tx, WR_FUNCTION);
            Buffer_addData(message_tx, (uint8_t)(sreg >> 8));
            Buffer_addData(message_tx, (uint8_t)(sreg & 0x00FF));
            Buffer_addData(message_tx, (uint8_t)(nreg >> 8));
            Buffer_addData(message_tx, (uint8_t)(nreg & 0x00FF));
            Buffer_addCRC(message_tx);
            Buffer_txMain(message_tx);
            break;

        default:
            // do nothing
            break;
    }
}


